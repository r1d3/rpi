##!/usr/bin/env python
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of R1D3.
#
# R1D3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# R1D3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with R1D3.  If not, see <http://www.gnu.org/licenses/>.
#

import sys
import logging

import sleekxmpp

# Python versions before 3.0 do not use UTF-8 encoding
# by default. To ensure that Unicode is handled properly
# throughout SleekXMPP, we will set the default encoding
# ourselves to UTF-8.
if sys.version_info < (3, 0):
    from sleekxmpp.util.misc_ops import setdefaultencoding
    setdefaultencoding('utf8')
else:
    raw_input = input


class CommandUserBot(sleekxmpp.ClientXMPP):

    """
    A simple SleekXMPP bot that uses the adhoc command
    provided by the adhoc_provider.py example.
    """

    def __init__(self, jid, password, other, cmd_bot, cmd_bot2):
        sleekxmpp.ClientXMPP.__init__(self, jid, password)

        self.command_provider = other
        self.cmd_bot = cmd_bot
        self.cmd_bot2 = cmd_bot2

        # The session_start event will be triggered when
        # the bot establishes its connection with the server
        # and the XML streams are ready for use. We want to
        # listen for this event so that we we can initialize
        # our roster.
        self.add_event_handler("session_start", self.start)
        self.add_event_handler("message", self.message)

    def start(self, event):
        """
        Process the session_start event.

        Typical actions for the session_start event are
        requesting the roster and broadcasting an initial
        presence stanza.

        Arguments:
            event -- An empty dictionary. The session_start
                     event does not provide any additional
                     data.
        """
        self.send_presence()
        self.get_roster()

        # We first create a session dictionary containing:
        #   'next'  -- the handler to execute on a successful response
        #   'error' -- the handler to execute if an error occurs

        # The session may also contain custom data.

        session = {'cmd_bot': cmd_bot,
                   'cmd_bot2': cmd_bot2,
                   'next': self._command_start,
                   'error': self._command_error}

        self['xep_0050'].start_command(jid=self.command_provider,
                                       node='r1dX',
                                       session=session)

    def message(self, msg):
        """
        Process incoming message stanzas.

        Arguments:
            msg -- The received message stanza.
        """
        logging.info(msg['body'])

    def _command_start(self, iq, session):
        """
        Process the initial command result.

        Arguments:
            iq      -- The iq stanza containing the command result.
            session -- A dictionary of data relevant to the command
                       session. Additional, custom data may be saved
                       here to persist across handler callbacks.
        """

        # The greeting command provides a form with a single field:
        # <x xmlns="jabber:x:data" type="form">
        #   <field var="greeting"
        #          type="text-single"
        #          label="Your greeting" />
        # </x>

        form = self['xep_0004'].makeForm(ftype='submit')
        form.addField(var='command',
                      value=session['cmd_bot'])

        session['payload'] = form

        # We need to process the next result.
        session['next'] = self['xep_0050'].continue_command(session)
        self.command_next(session)

        # Other options include using:
        # continue_command() -- Continue to the next step in the workflow
        # cancel_command()   -- Stop command execution.

    def command_next(self, session):

        form = self['xep_0004'].makeForm(ftype='submit')
        form.addField(var='command',
                      value=session['cmd_bot2'])

        session['payload'] = form

        # We don't need to process the next result.
        session['next'] = None

        self['xep_0050'].complete_command(session)

    def _command_error(self, iq, session):
        """
        Process an error that occurs during command execution.

        Arguments:
            iq      -- The iq stanza containing the error.
            session -- A dictionary of data relevant to the command
                       session. Additional, custom data may be saved
                       here to persist across handler callbacks.
        """
        logging.error("COMMAND: %s %s" % (iq['error']['condition'],
                                          iq['error']['text']))

        # Terminate the command's execution and clear its session.
        # The session will automatically be cleared if no error
        # handler is provided.
        self['xep_0050'].terminate_command(session)
        self.disconnect()


if __name__ == '__main__':
    # Setup the command line arguments.

    loglevel="INFO"
    jid = "admin_bot@qnap.local/tests"
    password = "password"
    other = "robot@qnap.local/bot"
    cmd_bot = "Changer de mode"
    cmd_bot2 = u"Détection de visages"

    # Setup logging.
    logging.basicConfig(level=loglevel,
                        format='%(levelname)-8s %(message)s')


    # Setup the CommandBot and register plugins. Note that while plugins may
    # have interdependencies, the order in which you register them does
    # not matter.
    xmpp = CommandUserBot(jid, password, other, cmd_bot, cmd_bot2)
    xmpp.register_plugin('xep_0030') # Service Discovery
    xmpp.register_plugin('xep_0004') # Data Forms
    xmpp.register_plugin('xep_0050') # Adhoc Commands

    # If you are working with an OpenFire server, you may need
    # to adjust the SSL version used:
    # xmpp.ssl_version = ssl.PROTOCOL_SSLv3

    # If you want to verify the SSL certificates offered by a server:
    # xmpp.ca_certs = "path/to/ca/cert"

    # Connect to the XMPP server and start processing XMPP stanzas.
    if xmpp.connect():
        # If you do not have the dnspython library installed, you will need
        # to manually specify the name of the server if it does not match
        # the one in the JID. For example, to use Google Talk you would
        # need to use:
        #
        # if xmpp.connect(('talk.google.com', 5222)):
        #     ...
        xmpp.process(block=True)
        print("Done")
    else:
        print("Unable to connect.")
