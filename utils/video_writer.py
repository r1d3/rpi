import cv2
from time import sleep
import numpy as np
import os

cap = cv2.VideoCapture(0)
if not cap.isOpened():
    raise IOError("Cannot open webcam")

codec = 0x47504A4D # MJPG
cap.set(cv2.CAP_PROP_FOURCC, codec)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)
cap.set(cv2.CAP_PROP_FPS, 20.0)

videoname = os.path.join('/tmp/', f"output.avi")
fourcc = cv2.VideoWriter_fourcc(*'MJPG')
video_writer = cv2.VideoWriter(videoname, fourcc, 20.0, (640, 480))

while True:
    ret, frame = cap.read()
    video_writer.write(frame)
    if cv2.waitKey(1) & 0xFF == 27:
        break

cap.release()
video_writer.release()
cv2.destroyAllWindows()
