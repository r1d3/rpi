#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of R1D3.
#
# R1D3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# R1D3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with R1D3.  If not, see <http://www.gnu.org/licenses/>.
#
# This file is based on mines.py : BreezySLAM Python with SLAMTECH RP A1 Lidar of Simon D. Levy.
#

from breezyslam.vehicles import WheeledVehicle

import numpy as np
import pandas as pd


# Method to load all from file
# Each line in the file has the format:
#
#  TIMESTAMP  ... Q1  Q1 ... Distances
#  (usec)                    (mm)
#  0          ... 2   3  ... 24 ... 
#  
# where Q1, Q2 are odometry values

def load_data(datadir, dataset):
    # RPLidarA1(Laser):
    # 360 degree and 360 measurments (scan_size)

    filename = '%s/%s.dat' % (datadir, dataset)
    print('Loading data from %s...' % filename)

    fd = open(filename, 'rt')

    timestamps = []
    scans = []
    scans_0 = []
    odometries = []

    while True:
        data_string = fd.readline()
        if len(data_string) == 0:
            break

        toks = data_string.split()[0:-1]  # ignore ''
        timestamp = int(float(toks[0]))
        odometry = timestamp, int(toks[1]), int(toks[2])
        #  The scans are saved after odometry: angle:distance
        unormalized_data = toks[3:]
        normalized_data, normalized_data_0 = normalize_rplidar_scan(unormalized_data)
        df = pd.DataFrame(normalized_data)
        df = df['distance'].interpolate(method='linear')
        lidar_interpolated = df.values.tolist()
        # delete angle 0
        lidar_interpolated.pop(0)
        normalized_data_0.pop(0)
        lidar_0 = [x['distance'] for x in normalized_data_0]
        timestamps.append(timestamp)
        scans.append(lidar_interpolated)
        scans_0.append(lidar_0)
        odometries.append(odometry)
    fd.close()

    return timestamps, scans_0, odometries


def normalize_rplidar_scan(unormalized_data):
    normalized_data = [{'angle': count, 'distance': np.nan} for count in range(361)]
    normalized_data_0 = [{'angle': count, 'distance': 0} for count in range(361)]
    # RPlidar does not gives always the same amount of scans.
    # Angles may slightly vary
    # This part aims to normalize the data.
    for angle_dist in unormalized_data:
        angle, distance = angle_dist.split(":")
        angle = float(angle)
        distance = float(distance)
        angle = int(angle)
        normalized_data[int(angle)]['distance'] = distance
        normalized_data_0[int(angle)]['distance'] = distance
    return normalized_data, normalized_data_0


# Class for MinesRover custom robot ------------------------------------------

class Rover(WheeledVehicle):

    def __init__(self):
        wheel_radius_millimeters = 85
        half_axle_length_millimeters = 200
        WheeledVehicle.__init__(self, wheel_radius_millimeters, half_axle_length_millimeters)
        self.ticks_per_cycle = 6533

    def __str__(self):
        return '<%s ticks_per_cycle=%d>' % (WheeledVehicle.__str__(self), self.ticks_per_cycle)

    def computePoseChange(self, odometry):
        return WheeledVehicle.computePoseChange(self, odometry[0], odometry[1], odometry[2])

    def extractOdometry(self, timestamp, left_wheel, right_wheel):
        # Convert microseconds to seconds, ticks to angles
        return timestamp / 1e6, \
               self._ticks_to_degrees(left_wheel), \
               self._ticks_to_degrees(right_wheel)

    def odometryStr(self, odometry):
        return '<timestamp=%d msec leftWheelTicks=%d rightWheelTicks=%d>' % \
               (odometry[0], odometry[1], odometry[2])

    def _ticks_to_degrees(self, ticks):
        return ticks * (360 / self.ticks_per_cycle)
