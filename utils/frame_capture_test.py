import numpy as np
import cv2

cap = cv2.VideoCapture(0)

# Define the codec and create VideoWriter object
#fourcc = cv2.VideoWriter_fourcc(*'DIVX')
fourcc = cv2.cv.CV_FOURCC(*'DIVX')
out = cv2.VideoWriter('/tmp/output.avi',fourcc, 20.0, (640,480))

face_cascade = cv2.CascadeClassifier('../cascades/cascades-available/lbpcascade_frontalface.xml')
s = './cascades/cascades-available/l'
eye_cascade = cv2.CascadeClassifier(s +  'haarcascade_eye_tree_eyeglasses.xml')
#constraint_face_cascade = cv2.CascadeClassifier('../cascades/cascades-available/haarcascade_constrainedFrontalFace.xml')

def find_contours(result_image):
        contours, _ = cv2.findContours(result_image,
                                       cv2.RETR_LIST,
                                       cv2.CHAIN_APPROX_SIMPLE)
        # Find contours of all shapes
        return contours

def process_contours_eyes(gray_contrasted_img):
    contours = find_contours(gray_contrasted_img)
    max_area = 750
    # Select biggest; drop the rest
    contours_sorted = sorted(contours, key=cv2.contourArea, reverse=True)
    resw = 640
    resh = 480
    if len(contours_sorted) > 0:
            cnt = contours_sorted[0]
            x, y, w, h = cv2.boundingRect(cnt)
            centroid_x = x + (w / 2)
            centroid_y = y + (h / 2)
            camera_range = round((16175.288 / w), 0)
            # Not needed; Just to display the difference
            # cv2.drawContours(frame, [cnt], -1, (0, 0, 255), 2)
            cv2.rectangle(gray_contrasted_img, (x, y), (x + w, y + h), (0, 255, 0), 2)
            area = w * h
            if area > max_area:
                ret = True
                frame_roi = gray_contrasted_img[y:y + h, x:x + w]
            cv2.imshow('Input', gray_contrasted_img)



while(cap.isOpened()):
    ret, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    #gray_contrast = cv2.equalizeHist(gray)
    # create a CLAHE object (Arguments are optional).
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
    gray_contrast  = clahe.apply(gray)
    got_your_eyes = False
    got_your_face = False
    if ret:

        faces = face_cascade.detectMultiScale(gray_contrast, scaleFactor=1.3, minSize=(30,30))
        for (x,y,w,h) in faces:
            cv2.rectangle(frame,(x,y),(x+w,y+h),(255,0,0),2)  # bleu
            x_min = x+ (w*0.15)
            x_max = x+(w*0.85)
            y_min = y+(0.05*h)
            y_max = y  + (0.9*h)

            #print(x_min,x_max,y_min,y_max)

            cv2.rectangle(frame,(int(x_min),int(y_min)),(int(x_max),int(y_max)),(0,255,0),1)
            # the ayes are not at the bottom of the picture
            # new Rect(r.x +r.width/8,(int)(r.y + (r.height/4.5)),r.width - 2*r.width/8,(int)( r.height/3.0))

            # If (x1,y1) and (x2,y2) are the two opposite vertices of plate you obtained, then simply use function:
            # roi = gray[y1:y2, x1:x2] that is your image ROI.

            roi_gray = gray_contrast[y_min:y_max, x_min:x_max]
            roi_color = frame[y_min:y_max, x_min:x_max]

            #faces = face_cascade.detectMultiScale(roi_gray, scaleFactor=1.3, minSize=(30,30))
            #for (x,y,w,h) in faces:
            #    cv2.rectangle(roi_gray,(x,y),(x+w,y+h),(255,0,255),1)  #



            got_your_face = True

            # process_contours_eyes(roi_gray)
            # eyes = eye_cascade.detectMultiScale(roi_gray,scaleFactor=1.1, minNeighbors=2,minSize=(30, 30))
            # for (ex,ey,ew,eh) in eyes:
            #     cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(255,255,255),2)
            #     print('eye')
            #     got_your_eyes = True

        # write the flipped frame
        #out.write(frame)
        cv2.imshow('frame',frame)
        #cv2.imshow('gray',gray)

#        constraint_faces =constraint_face_cascade.detectMultiScale(gray_contrast, scaleFactor=1.3, minSize=(30,30))
#        for (x,y,w,h) in constraint_faces:
#            cv2.rectangle(gray_contrast,(x,y),(x+w,y+h),(125,125,0),2)  #


        if got_your_face:
            cv2.imshow('1', roi_gray)
            cv2.imshow('gray_contrast',gray_contrast)
        # if got_your_eyes:
        #     cv2.imshow('2', roi_gray)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    else:
        break

# Release everything if job is finished
cap.release()
out.release()
cv2.destroyAllWindows()


