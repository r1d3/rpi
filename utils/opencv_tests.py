import numpy as np
import cv2

cap = cv2.VideoCapture(0)

def frame_grabber():
    print("OK")
    while 1:
        font = cv2.FONT_HERSHEY_SIMPLEX
        ret_val, img = cap.read()
        cv2.putText(img, 'OpenCV', (100, 200), font, 4, (255, 255, 255), thickness=2, lineType=cv2.LINE_AA,
                    bottomLeftOrigin=0)
        cv2.imshow('my webcam', img)


        if cv2.waitKey(1) == 27:
            break  # esc to quit
    cv2.destroyAllWindows()


if __name__ == '__main__':
    frame_grabber()