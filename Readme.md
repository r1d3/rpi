# Description
R1D3 is a project that aim to built a connected robot. It is based on a raspberry PI 2 and in the future an Arduino Mega will bring the motricity with the help of several motors.
The program is written in Python3 and the followings functionalities are already available. 


## V 0.2.x:

See the [related blog article](https://blog.agayon.be/what_have_I_done.html).

* Full motor controll of the Arduino throught serial commands
* Lidar Mapping with a [RPLidar A1M8](https://www.robotshop.com/eu/en/rplidar-a1m8-360-degree-laser-scanner-development-kit.html)
* Bluetooth support to control it with a PS4 controller
* Socket support to be controlled with a HTTP API (see https://gitlab.com/r1d3/rest_api) 
* multiprocessing capabilities to isolate the several loops (menu, OpenCV and XMPP, video, bluetooth, ...).
* Python 3, OpenCV 4.5 for image analysis
* [Slixmpp](https://lab.louiz.org/poezio/slixmpp) instead of Sleekxmpp for XMPP support

## V 0.1.x:
* Configuration though a `.ini` file.
* XMPP capabilities: 
  - connect to a server with a given jid.
  - connect to a multi user chat (MUC).
  - command the robot through AdHoc XMPP commands (see [here](http://xmpp.org/extensions/xep-0050.html)). Some examples of commands are:
        + change the mode of the robot (face detection, sign recognition, stop the OpenCV process, ...).   See the OpenCV capabilities.
        + send the status of the robot to a given jid.
        + disconnect
        + obtain the status of the robot (e.g. face detection, sign tracking, available) is given in the roster and the robot is marked as "busy"
  - ...
* OpenCV capabilities:
  - detect faces.
  - recognize faces after training
  - tell in the MUC "I am with ${recognized person} (confidence = [calculated confidence - the lower the better])"
  - detect a sign placed on a red panel (see [here](http://roboticssamy.blogspot.be/2013/11/rs4-robot-update-new-features.html)).
  - tell in the MUC that it found the sign.
  - ...




## Future versions
 The next version (V 0.3) will bring the following functionalities:
* Command the robot with a Char LCD plate (16x2) and a simple menu. It is connected by I2C (see [here](https://www.adafruit.com/products/1109)). The code is ready but the physical link is not ready
* Enhance the face recognition procedure
* Enhance the video file recording to avoid cuts
* Detect the kernel messages about battery. When the voltage is too low, the information is logged by the kernel and can be found with systemd. The idea would be to shut down the robot when necessary to avoid SD card memory corruption.

# Install and configuration
- You can install R1D3 by cloning the repository: `git clone https://gitlab.com/r1d3/RPI.git`.
- Then, you have to copy the file `r1d3.example.ini` to `r1d3.ini`.
- You have to edit it. The options are described.
- If you want the robot to use its OpenCV capabilities, you have to download some cascade files in XML format. You can found some of them in the following locations:
  + [fist](https://github.com/Aravindlivewire/Opencv/blob/master/haarcascade/fist.xml)  
  + [palm](https://github.com/Aravindlivewire/Opencv/blob/master/haarcascade/palm.xml)  
  + [closed frontal palm](https://github.com/Aravindlivewire/Opencv/blob/master/haarcascade/closed_frontal_palm.xml)  
  + [lbpcascade frontalface](https://github.com/Itseez/opencv/blob/master/data/lbpcascades/lbpcascade_frontalface.xml)  

- Copy the XML files in the following directory `cascades/cascades-available`.
- Create a symbolic link in the directory `cascades/cascades-enabled` for each cascade file you want to 
activate. For example, if you want R1D3 to detect your fist:
```
cd cascades/cascades-enabled
ln -s ../cascades/cascades-available/fist.xml
```
If you want R1D3 to recognize you, you have to train it. 
- Connect your webcam to a computer equipped with a display. 
- Launch the script `build_faces_library.py` in the utils directory. You have to press `1` to acquire and give your name.
- Press 'c' when the script detect your face. By default, 20 pictures are taken.
- Afterwhat you can test the recognition procedure by relaunching `build_faces_library.py` and press `2`. Don't forget to enable the cascade file `lbpcascade_frontalface.xml`
- When your training data set is satisfying, you can launch R1D3 by typing `python robot.py` in the root directory of the project. R1D3 will enter a standby mode. For now, you have to use the AdHoc command in order to change the mode (i.e. detect your face).

If you want the robot to communicate with XMPP, you have to create an account for it on a server and set the jid in the `r1d3.ini` file as well as it password.
For now, the MUC is mandatory.

# Requirement
+ Slixmpp
+ OpenCV 4.5

# Licence
R1D3 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

R1D3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with R1D3.  If not, see <http://www.gnu.org/licenses/>.
