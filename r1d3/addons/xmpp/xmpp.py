# -*- coding: utf-8 -*-

"""
    SleekXMPP: The Sleek XMPP Library
    Copyright (C) 2010  Nathanael C. Fritz
    This file is part of SleekXMPP.

    See the file LICENSE for copying permission.
"""
import logging
import slixmpp
import textwrap
import asyncio

logger = logging.getLogger('root')


class SendMsgBot(slixmpp.ClientXMPP):
    def __init__(self, d_mode, d_lcd, d_xmpp, mode_skeleton, jid, recipient, password, room, nick):
        slixmpp.ClientXMPP.__init__(self, jid, password)

        # Setup logging.
        logging.basicConfig(level=logging.INFO,
                            format='%(asctime)s - %(levelname)s - %(module)s - %(message)s')
        self.d_mode = d_mode
        self.d_lcd = d_lcd
        self.d_xmpp = d_xmpp
        self.mode_skeleton = mode_skeleton
        # The message we wish to send, and the JID that
        # will receive it.
        self.recipient = recipient
        self.room = room
        self.nick = nick
        self.mode = 'Faces'
        self.admin = recipient

        self.send_status = u''
        self.available = u''
        self.quit = u''
        self.admin_cmd = u''
        self.mode_1 = u''
        self.mode_2 = u''
        self.mode_3 = u''
        self.mode_4 = u''
        self.mode_5 = u''
        self.current_mode = u''
        self.reply_counter = {}

        # The session_start event will be triggered when
        # the bot establishes its connection with the server
        # and the XML streams are ready for use. We want to
        # listen for this event so that we we can initialize
        # our roster.
        self.add_event_handler("session_start", self.start)

        # The groupchat_presence event is triggered whenever a
        # presence stanza is received from any chat room, including
        # any presences you send yourself. To limit event handling
        # to a single room, use the events muc::room@server::presence,
        # muc::room@server::got_online, or muc::room@server::got_offline.
        self.add_event_handler("muc::%s::got_online" % self.room,
                               self.muc_online)

        # The groupchat_message event is triggered whenever a message
        # stanza is received from any chat room. If you also also
        # register a handler for the 'message' event, MUC messages
        # will be processed by both handlers.
        self.add_event_handler("groupchat_message", self.muc_message)

        # The message event is triggered whenever a message
        # stanza is received. Be aware that that includes
        # MUC messages and error messages.
        self.add_event_handler("message", self.recv_message)

    def get_dict_mode(self):
        return self.d_mode

    def set_to(self, to):
        self.recipient = to

    async def start(self, event):
        """
        Process the session_start event.

        Typical actions for the session_start event are
        requesting the roster and broadcasting an initial
        presence stanza.

        Arguments:
            event -- An empty dictionary. The session_start
                     event does not provide any additional
                     data.
        """
        #       self.plugin['xep_0092'].software_name = 'R1D3'
        #       self.plugin['xep_0092'].version = '1.0'

        await self['xep_0115'].update_caps()
        self.send_presence()
        self.get_roster()

        # self.plugin['xep_0045'].join_muc(self.room,
        #                                  self.nick,
        #                                  wait=False)

        self['xep_0050'].add_command(node='r1dX',
                                     name=u'Contrôle R1D3',
                                     handler=self._handle_commands)
        self['xep_0050'].add_command(node='r1dXMPP',
                                     name=u'Message LCD',
                                     handler=self._handle_commands_lcd)

        d_xmpp_empty = {'to': ' ', 'msg': ' ', 'status': ' ', 'status_msg': ' ', 'roger': '1', 'send_dict': '0',
                        'online': True}
        # while True:
        #     # TODO: test me once the LCD can be used again :-)
        #     msg = self.d_xmpp.get('msg')
        #     online = self.d_xmpp['online']
        #     update = False
        #     if msg.rstrip():
        #         if self.d_xmpp['to'] == 'muc':
        #             update = True
        #             self.send_muc(self.d_xmpp['msg'])
        #         if self.d_xmpp['to'] == 'recipient':
        #             update = True
        #             self.send(self.d_xmpp['msg'])
        #     if online is False:
        #         self.disconnect()
        #         break
        #     if update is True:
        #         logger.info("Send message from XMPP Manager dict")
        #         self.d_xmpp = update_manager_dict(self.d_xmpp, d_xmpp_empty)
        #     await asyncio.sleep(2)

    def _handle_commands(self, iq, session):
        """
        Handle the command being processed
        """
        form = self['xep_0004'].make_form('form', 'bot')
        form['instructions'] = u'Choisissez une option'
        self.send_status = "Status"
        self.available = 'Disponible'
        self.quit = u"Déconnection"
        self.lcd = 'Message LCD'
        self.admin_cmd = "Changer de mode"
        options = []
        # note 3
        if session['from'].bare == self.admin:
            options.append({'label': self.admin_cmd, 'value': self.admin_cmd})
        for o in [self.available, self.send_status, self.lcd, self.quit]:
            options.append({'label': o, 'value': o})
        options.append({'label': self.quit, 'value': self.quit})
        options.append({'label': "42", 'value': "42"})
        form.addField(var='command',
                      label=u'Commande',
                      ftype='list-single',
                      required=True,
                      options=options
                      )

        session['payload'] = form
        session['next'] = self._handle_command_next
        session['has_next'] = True
        return session

    def _handle_command_next(self, payload, session):
        """
        The next step for the menu
        """
        self.mode_0 = u"Détection de panneau"
        self.mode_1 = "Mapping"
        self.mode_2 = u"Détection de visages"
        self.mode_3 = "Suis moi"
        self.mode_4 = u"Stop capture et mapping"
        self.mode_5 = u"Camera capture without streaming"

        form = payload.get_values()
        command = form['command']
        # By default, admin_cmd = change of mode
        if command == self.admin_cmd:
            form = self['xep_0004'].make_form('form', 'bot')
            form['instructions'] = u'Choisissez un mode'
            options_list = [self.mode_4, self.mode_5, self.mode_0, self.mode_1, self.mode_2, self.mode_3]
            options = []
            for o in options_list:
                options.append({'label': o, 'value': o})
            form.addField(var='command',
                          label=u'Commande',
                          ftype='list-single',
                          required=True,
                          options=options
                          )

            session['payload'] = form
            session['has_next'] = True
            session['next'] = self._handle_command_complete

        elif command == self.quit:
            self.disconnect(wait=True)  # note 4
        elif command == self.available:
            self.send_presence(pstatus='I am OK', pshow='available')
            self.current_mode = u''
        elif command == self.send_status:
            mode = self.mode_skeleton
            status = u'\n'
            for k, v in mode.items():
                status += str(k) + ' : ' + str(v) + '\n'
            self.send_message(mto=session['from'],
                              mbody=status,
                              mtype='chat')

        if command == self.lcd:
            dict_lcd = {'msg': 'Coucou depuis\nXMPP !!! :-)', 'color': 'red', 'menu': 'continue'}
            self.d_lcd = update_manager_dict(self.d_lcd, dict_lcd)
            logger.debug("LCD message sent")
        if command == "42":
            self.send_message(mto=session['from'],
                              mbody="42",
                              mtype='chat')
        return session

    def _handle_command_complete(self, payload, session):
        """
        Handle the commands when clicking sur finish in gajim for example.
        """
        mode_skeleton = self.mode_skeleton
        form = payload.get_values()
        command = form['command']
        test_admin = self.is_admin(session['from'].bare)
        # Sign tracking
        if command == self.mode_0 and test_admin:
            session['notes'] = [('info', self.mode_0)]
            # available ; away ;chat = you want to talk;  dnd = busy and xa = extended away
            self.send_presence(pstatus=self.mode_0, pshow='dnd')
            self.current_mode = self.mode_0
            mode_skeleton['frame_capture'] = True
            mode_skeleton['sign_tracking'] = True
            mode_skeleton['shared_vars_from_xmpp'] = True

        # Mapping
        if command == self.mode_1 and test_admin:
            session['notes'] = [('info', self.mode_1)]
            # available ; away ;chat = you want to talk;  dnd = busy and xa = extended away
            self.send_presence(pstatus=self.mode_1, pshow='dnd')
            self.current_mode = self.mode_1
            mode_skeleton['mapping_mode'] = True
            mode_skeleton['shared_vars_from_xmpp'] = True

        # faces detection
        elif command == self.mode_2 and test_admin:
            session['notes'] = [('info', self.mode_2)]
            self.send_presence(pstatus=self.mode_2, pshow='dnd')
            self.current_mode = self.mode_2
            mode_skeleton['sign_tracking'] = False
            mode_skeleton['frame_capture'] = True
            mode_skeleton['facedetect_mode'] = True
            mode_skeleton['shared_vars_from_xmpp'] = True

        # follow me
        elif command == self.mode_3 and test_admin:
            session['notes'] = [('info', self.mode_3)]
            self.send_presence(pstatus=self.mode_3, pshow='dnd')
            self.current_mode = self.mode_3
            mode_skeleton['frame_capture'] = True
            mode_skeleton['sign_tracking'] = True
            mode_skeleton['shared_vars_from_xmpp'] = True
            mode_skeleton['facedetect_mode'] = False

        # stop the capture and other process
        elif command == self.mode_4 and test_admin:
            session['notes'] = [('info', self.mode_4)]
            self.send_presence(pstatus=self.mode_4, pshow='available')
            self.current_mode = self.mode_4
            mode_skeleton['frame_capture'] = False
            mode_skeleton['sign_tracking'] = False
            mode_skeleton['shared_vars_from_xmpp'] = False
            mode_skeleton['mapping_mode'] = False
            mode_skeleton['facedetect_mode'] = False
            mode_skeleton['video_reset'] = True

        # simple capture without pannel
        elif command == self.mode_5 and test_admin:
            session['notes'] = [('info', self.mode_5)]
            self.send_presence(pstatus=self.mode_5, pshow='available')
            self.current_mode = self.mode_5
            mode_skeleton['frame_capture'] = True
            mode_skeleton['video_mode'] = True
            mode_skeleton['sign_tracking'] = False
            mode_skeleton['shared_vars_from_xmpp'] = False
            mode_skeleton['mapping_mode'] = False
            mode_skeleton['facedetect_mode'] = False

        elif command == self.admin_cmd:
            # note 5
            if test_admin:
                session['notes'] = [('warning', 'BOOM !')]
            else:
                raise ValueError("ce n'est pas un admin !")

        self.d_mode = update_manager_dict(self.d_mode, mode_skeleton)

        return session

    def is_admin(self, from_who):
        ret = False
        if from_who == self.admin:
            ret = True
        return ret

    def _handle_commands_lcd(self, iq, session):
        """
        Handle the command being processed
        The user can send a message on the LCD char plate
        Respond to the intial request for a command.
        Arguments:
            iq      -- The iq stanza containing the command request.
            session -- A dictionary of data relevant to the command
                       session. Additional, custom data may be saved
                       here to persist across handler callbacks.
        """
        form = self['xep_0004'].make_form('form', 'Message LCD')
        form.addField(var='msg_lcd',
                      ftype='text-single',
                      label='Tappez un message: ')

        session['payload'] = form
        session['next'] = self._handle_command_complete_lcd
        session['has_next'] = True
        session['cancel'] = self.plugin['xep_0050']._handle_command_cancel

        # Other useful session values:
        # session['to']                    -- The JID that received the
        #                                     command request.
        # session['from']                  -- The JID that sent the
        #                                     command request.
        # session['has_next'] = True       -- There are more steps to complete
        # session['allow_complete'] = True -- Allow user to finish immediately
        #                                     and possibly skip steps
        # session['cancel'] = handler      -- Assign a handler for if the user
        #                                     cancels the command.
        # session['notes'] = [             -- Add informative notes about the
        #   ('info', 'Info message'),         command's results.
        #   ('warning', 'Warning message'),
        #   ('error', 'Error message')]
        return session

    def _handle_command_complete_lcd(self, payload, session):
        """
        Process a command result from the user.
        Arguments:
            payload -- Either a single item, such as a form, or a list
                       of items or forms if more than one form was
                       provided to the user. The payload may be any
                       stanza, such as jabber:x:oob for out of band
                       data, or jabber:x:data for typical data forms.
            session -- A dictionary of data relevant to the command
                       session. Additional, custom data may be saved
                       here to persist across handler callbacks.
        """

        # In this case (as is typical), the payload is a form
        form = payload
        msg_lcd = form.values['field']['value']
        msg = "Message de " + str(session['from']) + " envoyé au LCD: " + '\n' + msg_lcd
        # self.send_message(mto=session['from'],
        #                  mbody="%s, World!" % msg_lcd)
        self.send_muc(msg)
        # Having no return statement is the same as unsetting the 'payload'
        # and 'next' session values and returning the session.
        # Unless it is the final step, always return the session dictionary.
        if len(msg_lcd) < 17:
            dict_lcd = {'msg': msg_lcd, 'color': 'green', 'menu': 'continue'}
            self.d_lcd = update_manager_dict(self.d_lcd, dict_lcd)
        if 16 < len(msg_lcd) < 33:
            msg_1, msg_2 = self.cut_16(msg_lcd)
            dict_lcd = {'msg': msg_1 + '\n' + msg_2, 'color': 'green', 'menu': 'continue'}
            self.d_lcd = update_manager_dict(self.d_lcd, dict_lcd)
        if len(msg_lcd) > 32:
            lines = textwrap.wrap(msg_lcd, 32)
            for m in lines:
                msg_1, msg_2 = self.cut_16(m)
                logger.debug(msg_1 + ' *** ' + msg_2)
                dict_lcd = {'msg': msg_1 + '\n' + msg_2, 'color': 'green', 'menu': 'continue'}
                self.d_lcd = update_manager_dict(self.d_lcd, dict_lcd)
        logger.debug("LCD message sent " + msg)

        session['payload'] = None
        session['next'] = None
        return session

    def cut_16(self, msg):
        lines = textwrap.wrap(msg, 16)
        msg_1 = lines[0]
        msg_2 = ''
        if len(lines) > 1:
            msg_2 = lines[1]
        return msg_1, msg_2

    # #################################################################################

    def muc_message(self, msg):
        """
        Process incoming message stanzas from any chat room. Be aware
        that if you also have any handlers for the 'message' event,
        message stanzas may be processed by both handlers, so check
        the 'type' attribute when using a 'message' event handler.

        Whenever the bot's nickname is mentioned, respond to
        the message.

        IMPORTANT: Always check that a message is not from yourself,
                   otherwise you will create an infinite loop responding
                   to your own messages.

        This handler will reply to messages that mention
        the bot's nickname.

        Arguments:
            msg -- The received message stanza. See the documentation
                   for stanza objects and the Message stanza to see
                   how it may be used.
        """
        try:
            counter = self.reply_counter[msg['mucnick']]
        except KeyError:
            self.reply_counter[msg['mucnick']] = 0
            counter = 0
        if msg['mucnick'] != self.nick and self.nick in msg['body'] and counter < 1:
            self.send_message(mto=self.room,
                              mbody="Je t'entends , %s." % msg['mucnick'],
                              mtype='groupchat')
            self.reply_counter[msg['mucnick']] += 1

    def muc_online(self, presence):
        """
        Process a presence stanza from a chat room. In this case,
        presences from users that have just come online are
        handled by sending a welcome message that includes
        the user's nickname and role in the room.

        Arguments:
            presence -- The received presence stanza. See the
                        documentation for the Presence stanza
                        to see how else it may be used.
        """
        if presence['muc']['nick'] != self.nick:
            # mto=presence['from'].bare,
            self.send_message(mto=self.room,
                              mbody="Bonjour, %s %s" % (presence['muc']['role'],
                                                        presence['muc']['nick']),
                              mtype='groupchat')

    def recv_message(self, msg):
        if msg['type'] in ('chat', 'normal', 'groupchat'):
            if msg['mucnick'] != self.nick:
                print("message:%(body)s" % msg, msg['type'])
                # msg.reply("Thanks for sending\n%(body)s" % msg).send()

    def send_msg(self, msg):
        self.send_message(mto=self.recipient,
                          mbody=msg,
                          mtype='chat')  # normal or chat

    def send_muc(self, msg):
        m = 'send_muc: ' + msg + ' ' + self.room
        self.send_message(mto=self.room,
                          mbody=msg,
                          mtype='groupchat')

    def set_status(self, status, status_msg):
        self.send_presence(pstatus=status_msg, pshow=status)

    def send_status(self):
        print("TODO")
        msg = 'TODO'
        self.send_message(mto=self.recipient,
                          mbody=msg,
                          mtype='chat')  # normal or chat


def update_manager_dict(input_manager_dict, new_dict):
    dict_temp = input_manager_dict
    for key, value in new_dict.items():
        dict_temp[key] = value
    input_manager_dict = dict_temp
    return input_manager_dict


def init_xmpp(d_xmpp, d_mode, d_lcd, mode_skeleton, jid, to_jid,
              password, room, nick, xmpp_register_plugins):
    xmpp_instance = SendMsgBot(d_mode, d_lcd, d_xmpp, mode_skeleton, jid, to_jid, password, room, nick)
    # xmpp_instance.send_msg('R1D3 is in the place')
    logging.getLevelName('root')
    # split every xmpp_register_plugin and load them in a loop.
    for plugin in xmpp_register_plugins.split(" "):
        xmpp_instance.register_plugin(plugin)
    # this xep provide ping sending to keep the connection alive
    xmpp_instance.register_plugin('xep_0199', {'keepalive': True,
                                               'frequency': 15})

    # FIXME slixmpp does not allow to send message from d_xmpp for now.
    xmpp_instance.connect()
    xmpp_instance.process()
    d_xmpp_empty = {'to': ' ', 'msg': ' ', 'status': ' ', 'status_msg': ' ', 'roger': '1', 'send_dict': '0',
                    'online': True}
