#!/usr/bin/env python
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of R1D3.
#
# R1D3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# R1D3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with R1D3.  If not, see <http://www.gnu.org/licenses/>.
#


import serial
from serial.serialutil import SerialException
from time import sleep
import logging
import json
from json.decoder import JSONDecodeError
import os

logger = logging.getLogger('root')

# FIXME
#   File "/home/xuser/projects/r1d3/RPI/r1d3/addons/arduino_serial/arduino_serial.py", line 43, in listen
#     line = ser.readline()  # read a '\n' terminated line
#   File "/home/xuser/projects/r1d3/RPI/venv/lib/python3.8/site-packages/serial/serialposix.py", line 509, in read
#     raise SerialException('read failed: {}'.format(e))
# serial.serialutil.SerialException: read failed: device reports readiness to read but returned no data (device
# disconnected or multiple access on port?)
#

DATA_TYPE = {'EN': 'Encoder', 'UL': 'Ultrasonic sensor', 'MI': 'Minimu Measurment', 'CU': 'Counter Update'}


class ArduinoSerial:
    def __init__(self, d_mode, d_ser, d_slam, mode_skeleton, address, speed):
        logger.info("Init Serial")
        self.d_mode = d_mode
        self.d_ser = d_ser
        self.d_slam = d_slam
        self.mode_skeleton = mode_skeleton
        self.address = address
        self.speed = speed
        self.timeout = 1
        self.slam_dict = {'odometry': {'left': None, 'right': None, 'arduito_count': 0}}
        self.arduino_counter = 0
        self.batch_data = {0: {}}
        self.batch_string = ""

    def listen(self):
        with serial.Serial(self.address, self.speed, timeout=self.timeout) as ser:
            try:
                arduino_data = ser.readline()  # read a '\n' terminated line
            except SerialException as e:
                logger.error(str(e))
                return None
            if arduino_data:
                arduino_string = arduino_data.decode('utf-8')
                arduino_string = arduino_string.rstrip()
                if arduino_string:
                    msg = "Arduino String: " + str(arduino_string)
                    logger.info(msg)
                    self.batch_string += arduino_string
                    if arduino_string.endswith("-1"):
                        self.handle_data()
                        self.batch_string = ""
                        # release the Arduino
                        logger.info("Release the Arduino: rpi_halt = 0")
                        self.send("l=0")

    def send(self, data):
        with serial.Serial(self.address, self.speed, timeout=self.timeout) as ser:
            data += '\n'
            data_byte = data.encode('utf-8')
            ser.write(data_byte)
            logger.info("Serial sent " + data.rstrip())

    def handle_data(self):
        arduino_strings = self.batch_string.split(',')
        # Remove empty strings
        arduino_strings = [s for s in arduino_strings if s]
        if arduino_strings:
            for arduino_string in arduino_strings:
                data = self.data_to_dict(arduino_string)
                if data:
                    self.write_arduino_log(data)
                if data.get('er') and data.get('el'):
                    data_slam = self.slam_dict.copy()
                    data_slam['odometry']['arduito_count'] = self.arduino_counter
                    data_slam['odometry']['left'] = data.get('le')
                    data_slam['odometry']['right'] = data.get('re')
                    self.d_slam = update_manager_dict(self.d_slam, data_slam)
                if data.get('ac'):
                    self.arduino_counter = data.get('ac')
                    self.batch_data[self.arduino_counter] = {}
                else:
                    self.batch_data[self.arduino_counter].update(data)
                # TODO: data from the distance sensors and minimu
                # print(self.batch_data)

    def data_to_dict(self, arduino_string):
        data = arduino_string.split(":")
        data_dict = {}
        if len(data) > 0 and data:
            # formatted data
            try:
                d_type = data[0]
                data.pop(0)
                for kv in data:
                    key, value = kv.split('_')
                    data_dict[key] = float(value)
                log_msg = "Formatted data received: " + DATA_TYPE[d_type] + ' ' + str(data_dict)
                logger.info(log_msg)
            except (KeyError, ValueError) as e:
                logger.error("Problem when handling string " + arduino_string)
                pass
        return data_dict

    def write_arduino_log(self, data):
        logger.info("write arduino log")
        with open('./arduino.log', 'a') as logfile:
            logfile.writelines(str(data) + '\n')


def init_serial(d_mode, d_ser, d_slam, mode_skeleton, address, speed):
    ret = None
    while not ret:
        sleep(0.5)
        ret = os.path.isfile(address) or os.path.islink(address)
    remote_instance = ArduinoSerial(d_mode=d_mode, d_ser=d_ser, d_slam=d_slam, mode_skeleton=mode_skeleton,
                                    address=address,
                                    speed=speed)
    d_ser_empty = {'serial': None}
    timeout = 1
    with serial.Serial(address, speed, timeout=timeout) as ser:
        while True:
            remote_instance.listen()
            sleep(0.05)
            if d_ser.get('serial') and d_ser.get('data'):
                if 'r=' in d_ser.get('data') and not d_mode.get('remote'):
                    logger.info("Received remote data")
                    if not d_mode.get('remote'):
                        remote_instance.send('m=1')
                        mode_skeleton['remote'] = True
                        d_mode = update_manager_dict(d_mode, mode_skeleton)
                        sleep(0.1)
                remote_instance.send(d_ser['data'])
                d_ser = update_manager_dict(d_ser, d_ser_empty)
                sleep(0.05)


def update_manager_dict(input_manager_dict, new_dict):
    dict_temp = input_manager_dict
    for key, value in new_dict.items():
        dict_temp[key] = value
    input_manager_dict = dict_temp
    return input_manager_dict
