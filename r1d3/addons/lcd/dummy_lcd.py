#!/usr/bin/env python
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of R1D3.
#
# R1D3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# R1D3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with R1D3.  If not, see <http://www.gnu.org/licenses/>.
#
import logging

logger = logging.getLogger('root')


class Character_LCD_RGB_I2C:
    """Dummy LCD module when i2c is not availaible.
    """

    def __init__(self, i2c, columns, lines):
        """Initialize dummy lcd.
        """
        logger.info("start dummy lcd")

    def left_button(self):
        """dummy left button
        """
        logger.info("Left button")

    def up_button(self):
        """dummy up button
        """
        logger.info("up button")

    def down_button(self):
        """Dummy down button
        """
        logger.info("Down button")

    def right_button(self):
        """Dummy right button
        """
        logger.info("Right button")

    def select_button(self):
        """
        Dummy select button
        """

    logger.info("Select button")

    def message(self, msg):
        """
        Dummy lcd message
        :param msg:
        :return:
        """
        logger.info("Message")
