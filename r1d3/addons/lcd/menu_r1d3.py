#!/usr/bin/env python
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of R1D3.
#
# R1D3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# R1D3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with R1D3.  If not, see <http://www.gnu.org/licenses/>.
#

# from multiprocessing import Process, Queue
from time import sleep
import logging
try:
    import  adafruit_character_lcd.character_lcd_rgb_i2c as character_lcd
except ModuleNotFoundError:
    from . import dummy_lcd as character_lcd
import subprocess
from . import menu

log = logging.getLogger('root')


class MyCharPlate(character_lcd.Character_LCD_RGB_I2C):
    # LED colors
    RED = (100, 0, 0)
    GREEN = (0, 100, 0)
    BLUE = (0, 0, 100)
    YELLOW = (100, 100, 0)
    CYAN = (0, 100, 1)
    MAGENTA = (100, 0, 100)
    WHITE = (100, 100, 100)
    color = WHITE

    def color(self, color):
        red, green, blue = color
        self.color = (red, green, blue)


class R1D3Menu:
    def __init__(self):
        success = False
        attempt = 0
        print("I2C connection...")
        # while attempt < 10 or success == False:
        #     try:
        #         self.lcd_instance = MyCharPlate(busnum=1)
        #         success = True
        #     except IOError:
        #         success = False
        #         attempt += 1
        #         print("I2C error, attempt: {}/10".format(attempt))
        #         sleep(10)
        self.lcd_instance = MyCharPlate(busnum=1)

    def ret_lcd(self):
        return self.lcd_instance

    def r1d3_menu(self, d_lcd, d_mode, d_xmpp, mode_skeleton, loading):
        menu_bot = menu.Menu()
        # create some custom characters
        # http://www.quinapalus.com/hd44780udg.html
        self.lcd_instance.create_char(1, [0, 8, 12, 14, 12, 8, 0, 0])  # >
        self.lcd_instance.create_char(2, [0, 2, 6, 14, 6, 2, 0, 0])  # <
        self.lcd_instance.create_char(3, [0, 0, 31, 14, 4, 0, 0, 0])  # v
        self.lcd_instance.create_char(4, [27, 17, 10, 0, 4, 17, 17, 31])  # :-)
        self.lcd_instance.create_char(5, [27, 17, 10, 0, 4, 0, 31, 17])  # :-(
        #       self.lcd_instance.create_char(6, [0, 1, 3, 22, 28, 8, 0, 0])  # OK
        self.lcd_instance.create_char(6, [7, 5, 7, 0, 0, 0, 0, 0])  # °
        self.lcd_instance.create_char(7, [31, 31, 31, 31, 31, 31, 31, 31])  # square for initialisation

        # The menu can show strings, python expressions adn call a python method (cleander code)
        # topElement(Name , Type of content , Lower row content)

        top_robot = menu_bot.top_element("\x02 Robot      \x01", "STRING", '       \x03')
        top_webcam = menu_bot.top_element("\x02 Webcam      \x01", "STRING", '       \x03')
        top_xmpp = menu_bot.top_element("\x02 XMPP        \x01", "STRING", '       \x03')
        top_arduino = menu_bot.top_element("\x02 Arduino    \x01", "STRING", '       \x03')
        top_network = menu_bot.top_element("\x02 Reseau     \x01", "STRING", '       \x03')
        top_sys = menu_bot.top_element("\x02 Systeme    \x01", "STRING", '       \x03')
        top_mode = menu_bot.top_element("\x02 Mode      \x01", "STRING", '       \x03')

        sub_network1 = menu_bot.sub_element("Reseau>Signal", "PYTHON_method", "self.get_wifi_signal()")
        sub_network2 = menu_bot.sub_element("Reseau>SSID", "PYTHON_method", "self.get_ssid()")
        sub_network3 = menu_bot.sub_element("Reseau>Internet", "PYTHON_method", "self.get_internet_status()")

        sub_sys1 = menu_bot.sub_element("Systeme>CPU", "PYTHON_method", "self.get_cpu()")
        sub_sys2 = menu_bot.sub_element("Systeme>LOAD", "PYTHON_method", "self.get_load()")
        sub_sys3 = menu_bot.sub_element("Systeme>CPU Temp.", "PYTHON_method", "self.get_cpu_temperature()")
        sub_sys4 = menu_bot.sub_element("Systeme>RAM", "PYTHON_method", "self.get_memory()")
        sub_sys5 = menu_bot.sub_element("Systeme>Eteindre", "STRING", "Ok?")
        sub_sys6 = menu_bot.sub_element("Systeme>Redemarrer", "STRING", "Ok?")

        sub_bot1 = menu_bot.sub_element("Robot>Explorer", "STRING", 'On / Off')
        sub_bot2 = menu_bot.sub_element("Robot>Salon", "STRING", 'On / Off')
        sub_bot3 = menu_bot.sub_element("Robot>Cuisine", "STRING", 'On / Off')
        sub_bot4 = menu_bot.sub_element("Robot>Bureau", "STRING", 'On / Off')

        sub_webcam1 = menu_bot.sub_element("Webcam>Liaison", "STRING", "TODO")
        sub_webcam2 = menu_bot.sub_element("Webcam>Film", "STRING", "TODO")
        sub_webcam3 = menu_bot.sub_element("Webcam>What else?", "STRING", ": TODO")

        sub_xmpp1 = menu_bot.sub_element("XMPP>Test", "STRING", "Send Status")
        sub_xmpp2 = menu_bot.sub_element("XMPP>Connect", "STRING", "TODO")

        sub_mode1 = menu_bot.sub_element("Mode>Panneau", "STRING", "On / Off")
        sub_mode2 = menu_bot.sub_element("Mode>Visage", "STRING", "On / Off")
        sub_mode3 = menu_bot.sub_element("Mode>Carte", "STRING", "On / Off")
        sub_mode4 = menu_bot.sub_element("Mode>Repos", "STRING", "On / Off")

        # Adding elements to the menu
        menu_bot.add_top_element(top_robot)
        menu_bot.add_top_element(top_webcam)
        menu_bot.add_top_element(top_xmpp)
        menu_bot.add_top_element(top_mode)
        menu_bot.add_top_element(top_arduino)
        menu_bot.add_top_element(top_network)
        menu_bot.add_top_element(top_sys)

        menu_bot.add_sub_element(top_network, sub_network1)
        menu_bot.add_sub_element(top_network, sub_network2)
        menu_bot.add_sub_element(top_network, sub_network3)

        menu_bot.add_sub_element(top_webcam, sub_webcam1)
        menu_bot.add_sub_element(top_webcam, sub_webcam2)
        menu_bot.add_sub_element(top_webcam, sub_webcam3)

        menu_bot.add_sub_element(top_robot, sub_bot1)
        menu_bot.add_sub_element(top_robot, sub_bot2)
        menu_bot.add_sub_element(top_robot, sub_bot3)
        menu_bot.add_sub_element(top_robot, sub_bot4)

        menu_bot.add_sub_element(top_sys, sub_sys1)
        menu_bot.add_sub_element(top_sys, sub_sys2)
        menu_bot.add_sub_element(top_sys, sub_sys3)
        menu_bot.add_sub_element(top_sys, sub_sys4)
        menu_bot.add_sub_element(top_sys, sub_sys5)
        menu_bot.add_sub_element(top_sys, sub_sys6)

        menu_bot.add_sub_element(top_xmpp, sub_xmpp1)
        menu_bot.add_sub_element(top_xmpp, sub_xmpp2)

        menu_bot.add_sub_element(top_mode, sub_mode1)
        menu_bot.add_sub_element(top_mode, sub_mode2)
        menu_bot.add_sub_element(top_mode, sub_mode3)
        menu_bot.add_sub_element(top_mode, sub_mode4)

        color = self.lcd_instance.CYAN
        self.lcd_instance.color(color)
        self.lcd_instance.clear()
        # self.lcd_instance.message("Hello!  \x07 \x07 \x07")
        sleep(1)
        # Scroll test in from the Left
        # message1ToPrint = "Maintenant,"
        # message2ToPrint = "il ecrit!"
        # i=20
        # while i>=0:
        #     lcd.clear()
        #     lcd.setCursor(0,0)
        #     suffix = " " * i
        #     lcd.message(suffix + message1ToPrint, lcd.TRUNCATE)
        #     lcd.setCursor(0,1)
        #     lcd.message(suffix + message2ToPrint, lcd.TRUNCATE)
        #     sleep(.25)
        #     i -= 1
        # sleep(2)

        # sleep(1)
        self.lcd_instance.clear()
        if loading:
            # little loading animation
            i = 0
            self.lcd_instance.message("Initialisation\n")
            while i < 16:
                # self.lcd_instance.message(chr(219))
                self.lcd_instance.message('\x07')
                sleep(.1)
                i += 1

        # starting the menu
        # specials chars
        # self.lcd_instance.message('\x04  \x05 \x06 \x07')
        sleep(2)
        menu_bot.start_menu(d_lcd, d_mode, d_xmpp, mode_skeleton, self.lcd_instance, color)


def init_menu(d_lcd, d_mode, d_xmpp, mode_skeleton, msg_string, loading):
    lcd_menu = R1D3Menu()
    lcd_instance = lcd_menu.ret_lcd()
    lcd_instance.clear()
    lcd_instance.color(lcd_instance.YELLOW)
    lcd_instance.message(msg_string)
    sleep(2.5)
    color = lcd_instance.CYAN
    lcd_instance.color(color)
    lcd_menu.r1d3_menu(d_lcd, d_mode, d_xmpp, mode_skeleton, loading)


def lcd_start(d_lcd, d_mode, d_xmpp, mode_skeleton):
    flag = 0
    msg_string = "R1D3 is in the\nplace"
    loading = True
    while True:
        try:
            init_menu(d_lcd, d_mode, d_xmpp, mode_skeleton, msg_string, loading)
        except IOError:
            sleep(5)
            subprocess.call(['i2cdetect', '-y', '1'])
            sleep(5)
            flag += 1
            logger.error("IO Error LCD: n# {}".format(flag))
            msg_string = "Reconnecting I2C..."
            loading = False


