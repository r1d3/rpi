#!/usr/bin/env python
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of R1D3.
#
# R1D3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# R1D3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with R1D3.  If not, see <http://www.gnu.org/licenses/>.
#

import os
from time import sleep


class Handle:
    def __init__(self, d_lcd, d_mode, d_xmpp, mode_skeleton,lcd_instance, color):
        self.color_prev = color
        self.lcd_instance = lcd_instance
        self.d_xmpp = d_xmpp
        self.d_mode = d_mode
        self.mode_skeleton = mode_skeleton
        self.mode_skeleton_back = mode_skeleton
        self.d_lcd = d_lcd

    def action(self, e):
        # map the inputs to the function blocks
        options = {"Systeme>Eteindre": self.poweroff,
                   "Systeme>Redemarrer": self.reboot,
                   "Webcam>Liaison": self.plop,
                   "Robot>Explorer": self.robot,
                   "Robot>Salon": self.robot,
                   "Robot>Cuisine": self.robot,
                   "Robot>Bureau": self.robot,
                   "Mode>Panneau": self.sign_tracking,
                   "Mode>Visage": self.face,
                   "Mode>Carte": self.map,
                   "Mode>Repos": self.standby,
                   "XMPP>Test": self.xmpp_test

                   }
        try:
            options[e]()
            dict_new = self.mode_skeleton
            self.d_mode = update_manager_dict(self.d_mode, dict_new)
            self.mode_skeleton = self.mode_skeleton_back
        except KeyError as e:
            print('KeyError - reason "%s"' % str(e), "unknown")

    def sign_tracking(self):
        color = self.lcd_instance.YELLOW
        self.lcd_instance.color(color)
        #self.shared_vars.set_vars(frame_capture=True, sign_tracking=True, shared_vars_from_lcd=True)
        self.mode_skeleton['frame_capture'] = True
        self.mode_skeleton['sign_tracking'] = True
        self.mode_skeleton['shared_vars_from_lcd'] = True
        self.mode_skeleton['facedetect_mode'] = False


    def face(self):
        color = self.lcd_instance.WHITE
        self.lcd_instance.color(color)
 #       self.shared_vars.set_vars(frame_capture=True, facedetect_mode= True,shared_vars_from_lcd=True)
        self.mode_skeleton['frame_capture'] = True
        self.mode_skeleton['facedetect_mode'] = True
        self.mode_skeleton['shared_vars_from_lcd'] = True
        self.mode_skeleton['sign_tracking'] = False


    def map(self):
        color = self.lcd_instance.MAGENTA
        self.lcd_instance.color(color)
        #self.shared_vars.set_vars(frame_capture=True, mapping_mode=True, shared_vars_from_lcd=True)
        self.mode_skeleton['frame_capture'] = True
        self.mode_skeleton['mapping_mode'] = True
        self.mode_skeleton['shared_vars_from_lcd'] = True
        self.mode_skeleton['sign_tracking'] = False
        self.mode_skeleton['facedetect_mode'] = False

    def standby(self):
        color = self.lcd_instance.GREEN
        self.lcd_instance.color(color)
        self.mode_skeleton['frame_capture'] = False
        self.mode_skeleton['facedetect_mode'] = False
        self.mode_skeleton['video_reset'] = True
        self.mode_skeleton['sign_tracking'] = False
        self.mode_skeleton['mapping_mode'] = False
        self.mode_skeleton['shared_vars_from_lcd'] = False

    def xmpp_test(self):
        status = u'\n'
        for v in ('shared_vars_from_lcd', 'shared_vars_from_xmpp', 'video_mode', 'follow_mode', 'mapping_mode',
                      'facedetect_mode', 'sign_tracking', 'sign_phony', 'shared_vars_from_xmpp'):
            #status += v + ' : ' +  str(v_share[v]) + '\n'
            status = "TODO"
            color = self.lcd_instance.BLUE
            self.lcd_instance.color(color)
            a = {'to': 'recipient', 'msg': status, 'status': ' ', 'status_msg': ' ', 'send_dict': '0', 'roger': '0'}
            update_manager_dict(self.d_xmpp, a)

    def poweroff(self):
        color = self.lcd_instance.RED
        self.lcd_instance.clear()
        self.lcd_instance.color(color)
        os.system("sudo systemctl poweroff")

    @staticmethod
    def reboot():
        os.system("sudo systemctl reboot")


    def lcd_action(self):
        self.lcd_instance.clear()
        color = self.lcd_instance.YELLOW
        self.lcd_instance.color(color)
        sleep(1)
        self.lcd_instance.color(self.color_prev)


    def robot(self):
        self.lcd_instance.clear()
        color = self.lcd_instance.YELLOW
        self.lcd_instance.color(color)
        sleep(1)
        self.lcd_instance.color(self.color_prev)

    @staticmethod
    def plop():
        print("plop")

def update_manager_dict(input_manager_dict, new_dict):
        dict_temp = input_manager_dict
        for key, value in new_dict.items():
            dict_temp[key] = value
        input_manager_dict = dict_temp
        return input_manager_dict