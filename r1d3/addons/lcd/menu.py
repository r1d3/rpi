#!/usr/bin/env python
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of R1D3.
#
# R1D3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# R1D3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with R1D3.  If not, see <http://www.gnu.org/licenses/>.
#
# This file is inspired by the work of Daniel Juenger
# @author: Arnaud Joset
# inspired by Daniel Juenger, http://github.com/sleeepyjack


from time import sleep
#import commands
import subprocess
import logging
logger = logging.getLogger('root')
import os
try:
    import psutil
except ImportError:
    logging.error('No ps util available')
    psutil = None

from . import handle_menu


class Menu:
    menu = list()
    top = 0
    sub = 0
    count = 1000
    element = None
    is_interrupted = False
    isOn = True
    isOnCount = 0
    step_scroll = 0

    def __init__(self):
        self.h = None
        pass


    def to_mib(self, bytes, factor=1024**2, suffix=" Mo "):
        """ Convert number of  octets to megaoctets.

            10247*1024 bytes = 1 Mo
            :param factor:
            :param bytes:
            :param suffix:
            :param bytes:
        """
        mo = str(bytes / factor) + suffix
        return mo

    def get_memory(self):
        results = {}
        memory = psutil.virtual_memory()
        results['memory'] = '{used}/ {total} ({percent}%)'.format(
            used=self.to_mib(memory.active),
            total=self.to_mib(memory.total),
            percent=memory.percent,
            available=memory.available
        )
        mem_usage = results['memory']
        mem_string = "%s" % mem_usage
        return mem_string

    def get_cpu(self):
        results = {'cpus': tuple("%s%%" % x for x in psutil.cpu_percent(percpu=True))}
        m = ""
        for i in results['cpus']:
            m += i + ' '
        return m

    def get_load(self):
        load = os.getloadavg()
        load_string = "%s %s %s" % load
        return load_string

    def get_cpu_temperature(self):
        with open('/sys/class/thermal/thermal_zone0/temp', 'r') as f:
            lines = f.readlines()
        temp = lines[0]
        temp = int(temp)
        temp /= 1000
        temp = str(temp) + '\x06' + 'C'
        return temp
    def get_wifi_signal(self):
        c = "iwconfig wlan0 | awk -F'[ =]+' '/Signal level/ {print $7}' | cut -d/ -f1"
        try:
            stdout = self.get_status_output(c)
        except TypeError:
            return "Cannot get Wifi Signal"
        return stdout

    def get_ssid(self):
        c = "iwconfig wlan0 | grep 'ESSID:' | awk '{print $4}' | sed 's/ESSID://g'"
        try:
            stdout = self.get_status_output(c)
        except TypeError:
            return 'Cannot get SSID'
        return stdout
    def get_internet_status(self):
        c = "ping -q -w 1 -c 1 `ip r | grep default | cut -d ' ' -f 3` > /dev/null && echo ok || echo error"
        try:
            stdout = self.get_status_output(c)
        except TypeError:
            return "Cannot get internet status"
        return stdout

    def get_status_output(self, *args, **kwargs):
        p = subprocess.Popen(*args, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True)
        #stdout, stderr = p.communicate()
        stdout = p.stdout.read()
        #return p.returncode, stdout, stderr
       #  print("DEBUG", p.returncode, stdout, stderr)
        return stdout

#############################################################################
    def top_element(self, name, type_of_entry, content):
        sublist = list()
        sel = self.sub_element(name, type_of_entry, content)
        sublist.append(sel)
        return {
            "Name": name,
            "Sub": sublist,
            "Type": type_of_entry,
            "Content": content}

    def sub_element(self, name, type_of_entry, content):
        return {
            "Name": name,
            "Type": type_of_entry,
            "Content": content}

    def button_pressed(self, lcd):
        boo = False
        if (lcd.is_pressed(lcd.SELECT) | lcd.is_pressed(lcd.UP) | lcd.is_pressed(lcd.DOWN) |
                lcd.is_pressed(lcd.LEFT) | lcd.is_pressed(lcd.RIGHT)):
            boo = True
        return boo

    def clear_menu_right(self, lcd):
        j = 0
        while j < 16:
            lcd.move_right()
            sleep(.03)
            j += 1

    def clear_menu_left(self, lcd):
        i = 0
        while i < 16:
            lcd.move_left()
            sleep(.03)
            i += 1

    def return_to_top_element(self):
        global element
        self.sub = 0
        self.element = self.menu[self.top]

    def first_top_element(self):
        global element
        self.top = 0
        self.sub = 0
        self.element = self.menu[self.top]
        return self.element

    def add_top_element(self, topel):
        if not topel in self.menu:
            self.menu.append(topel)

    def add_sub_element(self, topel, subel):
        if not subel in topel["Sub"]:
            topel["Sub"].append(subel)

    def return_element(self):
        global element
        return self.element

    def scroll(self, lcd, msg):
        global count
        global step_scroll
        if len(msg) > 16:
            self.step_scroll = len(msg) - 16
            if self.count <= 10 & self.step_scroll <= 0:
                lcd.move_left()
                self.step_scroll -= 1
            if self.count > 10:
                print("la")

    def next_top_element(self, lcd):
        global element
        global count
        self.clear_menu_left(lcd)
        self.count = 1000
        if len(self.menu) > 0:
            self.top = (self.top + 1) % len(self.menu)
            self.sub = 0
            self.element = self.menu[self.top]
        self.handle_menu(lcd)

    def prev_top_element(self, lcd):
        global element
        global count
        self.clear_menu_right(lcd)
        self.count = 1000
        if len(self.menu) > 0:
            self.top -= 1
            self.sub = 0
            if self.top < 0:
                self.top = len(self.menu) - 1
            self.element = self.menu[self.top]
        self.handle_menu(lcd)

    def next_sub_element(self, lcd):
        global element
        global count
        topel = self.menu[self.top]
        self.count = 1000
        if len(topel["Sub"]) > 0:
            self.sub += 1
            if self.sub >= len(topel["Sub"]):
                self.sub = 0
            self.element = topel["Sub"][self.sub]
        self.handle_menu(lcd)

    def prev_sub_element(self, lcd):
        global element
        global count
        topel = self.menu[self.top]
        self.count = 1000
        if len(topel["Sub"]) > 0:
            self.sub -= 1
            if self.sub < 0:
                self.sub = len(topel["Sub"]) - 1
            self.element = topel["Sub"][self.sub]
        self.handle_menu(lcd)

    def handle_menu(self, lcd):
        global element
        global count
        global isOn
        msg = ""
        if (self.count > 10) & self.isOn:
            if self.element["Type"] == "STRING":
                msg = self.element["Content"]
            elif self.element["Type"] == "PYTHON":
                msg = str(eval(self.element["Content"]))
            elif self.element["Type"] == "PYTHON_method":
                msg = "{}".format(eval(self.element["Content"]))

            self.count = 0
            lcd.clear()
            lcd.message(self.element["Name"] + "\n" + msg)
            self.scroll(lcd, msg)
        self.count += 1

    def start_menu(self, d_lcd, d_mode, d_xmpp, mode_skeleton, lcd_instance, color):
        global isInterrupted
        global isOn
        global isOnCount
        lcd_instance.clear()
        lcd_instance.color(color)
        self.h = handle_menu.Handle(d_lcd, d_mode, d_xmpp,mode_skeleton,lcd_instance, color)
        self.isOnCount = 0
        self.first_top_element()
        self.handle_menu(lcd_instance)
        self.isOn = True
        self.isOnCount = 0
        self.is_interrupted = False
        while not self.is_interrupted:
            try:
                if self.isOn:

                    if d_lcd['menu'] == 'stop':
                        self.is_interrupted = True
                    if d_lcd['color'] != ' ':
                        self.color_change(d_lcd['color'], lcd_instance)
                        self.isOnCount = 0
                    if d_lcd['msg'] != ' ':
                        lcd_instance.clear()
                        lcd_instance.message(d_lcd['msg'])
                        sleep(10)
                        dict_lcd = {'msg':' ', 'color':' ', 'menu':' '}
                        d_lcd = update_manager_dict(d_lcd, dict_lcd)
                        self.isOnCount = 0

                    lcd_instance.color(color)
                    if lcd_instance.is_pressed(lcd_instance.RIGHT):
                        self.next_top_element(lcd_instance)
                        self.isOnCount = 0
                        sleep(.3)
                    if lcd_instance.is_pressed(lcd_instance.LEFT):
                        self.prev_top_element(lcd_instance)
                        self.isOnCount = 0
                        sleep(.3)
                    if lcd_instance.is_pressed(lcd_instance.DOWN):
                        self.next_sub_element(lcd_instance)
                        self.isOnCount = 0
                        sleep(.3)
                    if lcd_instance.is_pressed(lcd_instance.UP):
                        self.prev_sub_element(lcd_instance)
                        self.isOnCount = 0
                        sleep(.3)
                    if lcd_instance.is_pressed(lcd_instance.SELECT):
                        e = self.return_element()
                        n = e["Name"]
                        self.h.action(n)
                        sleep(.3)

                        # self.returnToTopElement()
                        #              			self.isOnCount = 0
                        sleep(.3)
                    if self.isOnCount > 300:
                        lcd_instance.set_backlight(0.0)
                        lcd_instance.enable_display(False)
                        self.isOnCount = 0
                        self.isOn = False
                        sleep(.3)

                else:
                    if self.button_pressed(lcd_instance):
                        lcd_instance.enable_display(True)
                        lcd_instance.color(color)
                        self.isOnCount = 0
                        self.isOn = True
                        self.handle_menu(lcd_instance)
                        sleep(.3)
                self.handle_menu(lcd_instance)
                sleep(.3)
                self.isOnCount += 1
            except KeyboardInterrupt:
                self.stop_menu(lcd_instance)


    def stop_menu(self, lcd):
        global isInterrupted
        lcd.enable_display(False)
        self.is_interrupted = True

    def color_change(self, color_string, lcd_instance):
        """
        This function change the color of the LCD pannel
        :param color_string: the color of the dictionary
        :param lcd_instance: the lcd class instance
        :return: the color in the lcd class instance
        """
        # todo: a function to chose the color like handle_menu
        color = lcd_instance.YELLOW
        # We can't use the expression color1 'is' color2 because
        # their identity is different, they don't occupy the same memory adress.
        # they have different identity but they are equal.
        if color_string == 'teal':
            color = lcd_instance.TEAL
        if color_string == 'red':
            color = lcd_instance.RED
        if color_string == 'blue':
            color = lcd_instance.BLUE
        if color_string == 'yellow':
            color = lcd_instance.YELLOW
        if color_string == 'green':
            color = lcd_instance.GREEN
        if color_string == 'violet':
            color = lcd_instance.VIOLET
        if color_string == 'white':
            color = lcd_instance.WHITE
        lcd_instance.color(color)
        return color

def update_manager_dict(input_manager_dict, new_dict):
        dict_temp = input_manager_dict
        for key, value in new_dict.items():
            dict_temp[key] = value
        input_manager_dict = dict_temp
        return input_manager_dict