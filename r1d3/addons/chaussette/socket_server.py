#!/usr/bin/env python
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of R1D3.
#
# R1D3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# R1D3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with R1D3.  If not, see <http://www.gnu.org/licenses/>.
#
import socket
import os
import logging
import time
import subprocess
import copy

logger = logging.getLogger('root')

SOCKET = "/tmp/agayon_socket"


class Chaussette:
    def __init__(self, d_ser, mode_skeleton, d_mode, d_xmpp, d_lcd):
        logger.info("Opening socket...")
        if os.path.exists(SOCKET):
            os.remove(SOCKET)
        self.server = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
        self.server.bind(SOCKET)
        self.d_mode = d_mode
        self.d_ser = d_ser
        self.mode_skeleton = mode_skeleton
        self.d_xmpp = d_xmpp
        self.d_lcd = d_lcd

    def listen(self, ):
        while True:
            dict_ser = {'serial': None, 'data': {}}
            datagram = self.server.recv(1024)
            if not datagram:
                break
            else:
                data = datagram.decode('utf-8')
                logger.info(data)
                if "DONE" == data:
                    break
                elif "up" == data:
                    dict_ser = {'serial': True, 'data': 'r=1'}
                elif "down" == data:
                    dict_ser = {'serial': True, 'data': 'r=2'}
                elif "right" == data:
                    dict_ser = {'serial': True, 'data': 'r=3'}
                elif "left" == data:
                    dict_ser = {'serial': True, 'data': 'r=4'}
                elif "stopRobot" == data:
                    dict_ser = {'serial': True, 'data': 'r=5'}
                elif "smallRight" == data:
                    dict_ser = {'serial': True, 'data': 'a=1'}
                elif "smallLeft" == data:
                    dict_ser = {'serial': True, 'data': 'a=2'}
                elif "startStream" == data:
                    self.start_stream()
                elif "stopStream" == data:
                    self.stop_stream()
                elif "RPIrecord" == data:
                    self.movie_record()
                elif "lidarMapping" == data:
                    logger.info("Lidar Mapping: scan from socket")
                    skeleton = copy.deepcopy(self.mode_skeleton)
                    skeleton['mapping_mode'] = True
                    self.d_mode = update_manager_dict(self.d_mode, skeleton)
                self.d_ser = update_manager_dict(self.d_ser, dict_ser)
            time.sleep(0.1)

    def stop(self):
        self.server.close()
        os.remove(SOCKET)
        logger.info("Socket stopped")

    def start_stream(self):
        logger.info("start stream from websocket")
        self.mode_skeleton['stream'] = True
        self.mode_skeleton['frame_capture'] = False
        self.mode_skeleton['video_reset'] = True
        self.mode_skeleton['video_mode'] = False
        self.d_mode = update_manager_dict(self.d_mode, self.mode_skeleton)
        time.sleep(1)
        args = ["/usr/local/mybin/restart_stream.sh"]
        subprocess.run(args, capture_output=False)

    def stop_stream(self):
        logger.info("Stop stream from websocket")
        self.mode_skeleton['stream'] = False
        self.mode_skeleton['frame_capture'] = True
        self.mode_skeleton['video_mode'] = False
        self.mode_skeleton['video_reset'] = True
        self.d_mode = update_manager_dict(self.d_mode, self.mode_skeleton)
        time.sleep(1)
        args = ["/usr/local/mybin/stop_stream.sh"]
        subprocess.run(args, capture_output=False)

    def movie_record(self):
        self.stop_stream()
        time.sleep(1)
        self.mode_skeleton['frame_capture'] = True
        self.mode_skeleton['video_mode'] = True
        self.mode_skeleton['video_reset'] = False
        self.d_mode = update_manager_dict(self.d_mode, self.mode_skeleton)
        time.sleep(1)


def init_socket(d_xmpp, d_mode, d_lcd, d_ser, mode_skeleton, d_socket):
    socket_instance = Chaussette(d_ser=d_ser, mode_skeleton=mode_skeleton, d_mode=d_mode,
                                 d_xmpp=d_xmpp, d_lcd=d_lcd)
    socket_instance.listen()
    # while true here and listen regulary ?


def update_manager_dict(input_manager_dict, new_dict):
    dict_temp = input_manager_dict
    for key, value in new_dict.items():
        dict_temp[key] = value
    input_manager_dict = dict_temp
    return input_manager_dict
