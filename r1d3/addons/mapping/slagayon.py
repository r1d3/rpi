#!/usr/bin/env python3

##!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
# @author: Arnaud Joset
#
# This file is part of R1D3.
#
# R1D3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# R1D3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with R1D3.  If not, see <http://www.gnu.org/licenses/>.

# This file is based on rpslam.py : BreezySLAM Python with SLAMTECH RP A1 Lidar of Simon D. Levy.
#
"""

import logging
from time import sleep
from rplidar import RPLidar as Lidar
from rplidar import RPLidarException
import matplotlib.pyplot as plt
import numpy as np

logger = logging.getLogger('slam')

DISTANCE_MAX = 4000
IMIN = 0
IMAX = 50


class SLAM:
    def __init__(self, lidar_device, d_xmpp, map_size_pixels, map_size_meters, min_sample, output_file):
        logger.info('Starting SLAM')
        self.lidar_device = lidar_device
        self.min_sample = min_sample
        self.outpout_file = output_file
        self.info_health = None
        self.d_xmpp = d_xmpp

    def scan(self, odometry):
        """
        Scan data from the lidar and save it with odometry data
        :param odometry: dictionnary from Arduino with odometry data: left and right encoder from motors (Arduino)
        :return: Data with the same format as https://github.com/simondlevy/BreezySLAM used in the examples
         #
        # Each line in the file has the following format:
        #  TIMESTAMP  ... Q1  Q1 ... Angle:Distances
        #  (usec)                    (degree:mm)
        #  0          ... 2   3  ...  variable ...
        #
        # where Q1, Q2 are odometry values for left and right wheel respectively
        """
        try:
            lidar = Lidar(self.lidar_device)
        except RPLidarException as e:
            logger.error("Error at Lidar initialization")
            logger.error(e)
            return False

        # Create an iterator to collect scan data from the RPLidar
        if not self.info_health:
            try:
                info = lidar.get_info()
                health = lidar.get_health()
                msg_string = f"Lidar RPLidar A1M8. Serial {info['serialnumber']}, Health {health[0]}"
                d_xmpp_slam = {'to': 'recipient', 'msg': msg_string}
                self.d_xmpp = update_manager_dict(self.d_xmpp, d_xmpp_slam)
                logger.info(msg_string)
                self.info_health = True
            except RPLidarException as e:
                logger.error(e)
                logger.error("Error at Lidar initialization")
                lidar.stop()
                lidar.stop_motor()
                return False
        logger.info("Starting Scan")
        iterator = lidar.iter_scans()
        # We will use these to store previous scan in case current scan is inadequate
        timestamp = odometry and odometry['arduito_count'] or 0
        success = True
        # First scan is crap, so ignore it
        next(iterator)
        for count in range(10):
            # Items = # Extrat (quality, angle, distance) triples from current scan
            items = [item for item in next(iterator)]
            # Extract distances and angles from triples
            distances = [item[2] for item in items]
            angles = [item[1] for item in items]

            dist_str = " "
            for i in range(len(distances)):
                dist_str += "%s:%s " % (angles[i], distances[i])
            with open(self.outpout_file, 'a') as output_file:
                odometry_left = odometry and odometry['left'] or 0
                odometry_right = odometry and odometry['right'] or 0
                data_line = [str(timestamp), str(odometry_left), str(odometry_right)]
                data_str = " ".join(data_line) + dist_str
                logger.info("Writing scan %s / 10" % count)
                output_file.writelines(data_str + '\n')
        lidar.stop()
        lidar.stop_motor()
        # Save a picture of the last scan
        fig = plt.figure()
        ax1 = fig.add_subplot(111, projection='polar')
        line = ax1.scatter([0, 0], [0, 0], s=5, c=[IMIN, IMAX],
                           cmap=plt.cm.Greys_r, lw=0)
        offsets = np.array([(np.radians(meas[1]), meas[2]) for meas in items])
        line.set_offsets(offsets)
        intensity = np.array([meas[0] for meas in items])
        line.set_array(intensity)
        ax1.set_rmax(DISTANCE_MAX)
        ax1.grid(True)
        plt.title('R1D3 environment')
        filename = "/srv/http/ngnix/r1d3/public_html/static/lidar.png"
        plt.savefig(filename)
        logger.info(f"Saved scan {filename}")
        # arj fixme: take a picture with the camera. Check if recording and tells to save image here
        # Stop movie, take picture and reset movie
        return success


def slam_start(d_lcd, d_mode, d_xmpp, mode_skeleton, d_slam, d_ser,
               slam, map_size_pixel, map_size_meters, device_address, min_samples, output_file):
    """
    Start a slam instance and map the place when asked.
    """
    logger.info("INIT SLAM")
    slam_instance = None
    d_slam_empty = {'odometry': None}
    serial_dict = {'serial': True, 'data': 'l=0', 'desc': 'reset state arduino to continue walking'}
    while slam:
        if not slam_instance:
            slam_instance = SLAM(lidar_device=device_address, map_size_pixels=map_size_pixel,
                                 map_size_meters=map_size_meters,
                                 min_sample=min_samples, output_file=output_file, d_xmpp=d_xmpp)
        odometry = d_slam.get('odometry')
        slam_cond = odometry or d_mode.get('mapping_mode')
        if slam_cond and slam_instance:
            ret = slam_instance.scan(d_slam.get('odometry'))
            if ret:
                logger.info("Scan save sucessfully")
                d_ser = update_manager_dict(d_ser, serial_dict)
            d_slam = update_manager_dict(d_slam, d_slam_empty)
            if d_mode.get('mapping_mode'):
                d_mode = update_manager_dict(d_mode, mode_skeleton)
        sleep(0.2)


def update_manager_dict(input_manager_dict, new_dict):
    dict_temp = input_manager_dict
    for key, value in new_dict.items():
        dict_temp[key] = value
    input_manager_dict = dict_temp
    return input_manager_dict


if __name__ == '__main__':
    slam_start(d_lcd=None, d_mode=None, d_xmpp=None, mode_skeleton=None)
