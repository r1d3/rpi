#!/usr/bin/env python
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of R1D3.
#
# R1D3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# R1D3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with R1D3.  If not, see <http://www.gnu.org/licenses/>.
#


import cv2
import logging
import time
import numpy as np


class Tracking:
    def __init__(self, ser, window):
        self.ser = ser  # serial
        self.window = window
        self.roipts = []

    def boom_tracking(self, selection, trackobject):
        selection = (0, 0, 0, 0)
        trackobject = 0
        return [selection, trackobject]

    def seek_and_destroy(self, frame, trackobject, selection, sc):
        # tracking system
        logger = logging.getLogger('root')
        t1 = 0  # if nothing tracked
        # initialize the termination criteria for cam shift, indicating
        # a maximum of ten iterations or movement by a least one pixel
        # along with the bounding box of the ROI
        # termination = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 1)
        termination = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_MAX_ITER, 10, 1)
        termination = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_MAX_ITER, 100, 10)
        roibox = None
        x = selection[0]
        y = selection[2]
        w = selection[1]
        h = selection[3]

        # determine the top-left and bottom-right points
        self.roipts = [(x, y), (x, y + h), (x + w, y + h), (x + w, y)]
        self.roipts = np.array(self.roipts)
        s = self.roipts.sum(axis=1)
        # find the edges of the box
        tl = self.roipts[np.argmin(s)]
        br = self.roipts[np.argmax(s)]

        # grab the ROI for the bounding box and convert it
        # to the HSV color space
        roi = frame[tl[1]:br[1], tl[0]:br[0]]
        try:
            roi = cv2.cvtColor(roi, cv2.COLOR_BGR2HSV)
        # roi = cv2.cvtColor(roi, cv2.COLOR_BGR2LAB)

            # compute a HSV histogram for the ROI and store the
            # bounding box
            roihist = cv2.calcHist([roi], [0], None, [16], [0, 180])
            roihist = cv2.normalize(roihist, roihist, 0, 255, cv2.NORM_MINMAX)
            roibox = (tl[0], tl[1], br[0], br[1])
        except cv2.error:
            pass

        # see if the roi box has been computed
        if roibox is not None:
            # convert the current frame to the HSV color space
            # and perform mean shift
            hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
            backproj = cv2.calcBackProject([hsv], [0], roihist, [0, 180], 1)

            # apply cam shift to the back projection, convert the
            # points to a bounding box, and then draw them
            (r, roibox) = cv2.CamShift(backproj, roibox, termination)
            pts = np.int0(cv2.boxPoints(r))
            cv2.polylines(frame, [pts], True, (255, 0, 0), 2)
            font = cv2.FONT_HERSHEY_SIMPLEX
            cv2.putText(frame, "Tracking !", (int(round(x + w / 2)), int(round((y + h / 2)))), font,
                        1, (255, 255, 255), thickness=2, lineType=cv2.LINE_AA, bottomLeftOrigin=0)

            if self.window:
                # show the frame  and if the user presses a key
                cv2.imshow('Video', frame)
                cv2.imshow('ROI', roi)
                cv2.imshow('backproj', backproj)
                key = cv2.waitKey(1) & 0xFF

        if selection[1] < 1 or selection[2] < 1:
            logger.info("Lost tracked object.")
            selection, trackobject = self.boom_tracking(selection, trackobject)
            t1 = time.time()
        return [t1, selection, trackobject]
