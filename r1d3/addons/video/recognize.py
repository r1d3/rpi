#!/usr/bin/env python
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of R1D3.
#
# R1D3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# R1D3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with R1D3.  If not, see <http://www.gnu.org/licenses/>.
#

import cv2
import numpy as np
import os
import logging

IMAGE_SIZE = 170


class Recognize:
    def __init__(self, image_path, face_trainset, max_nbr_of_rec, d_lcd):
        self.image_path = image_path
        self.identities = []
        self.images = []  # X
        self.images_index = []  # Y
        self.camera = None
        self.classifier = cv2.CascadeClassifier(face_trainset)
        self.model = None
        self.max_nbr_of_rec = max_nbr_of_rec
        self.confidence_sum = 0
        self.dict_xmpp = {'to': ' ', 'msg': ' ', 'status': ' ', 'status_msg': ' ','roger': '1'}
        self.d_lcd = d_lcd

    def update_d_xmpp(self):
        return self.dict_xmpp

    def frame_process(self, frame, face_box, window, nbr_of_rec, peoples):
        if window:
            cv2.namedWindow("Video")
        x = face_box[0]
        w = face_box[1]
        y = face_box[2]
        h = face_box[3]
        ret = False
        resized = self.extract_and_resize(frame, x, y, w, h)
        identity, confidence = self.identity(resized)
        cv2.putText(frame, "%s (%s)" % (identity, confidence), (x, y),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255, 0, 0))
        self.confidence_sum += confidence
        if nbr_of_rec > 0:
            confidence_avg = (self.confidence_sum)/nbr_of_rec
        if nbr_of_rec > self.max_nbr_of_rec:  # we have enough proof that X is in the place
            if identity not in peoples and confidence_avg < 120:
                 # FIXME: number of rec may be shared between several persons.
                peoples.append(identity)
                # confidence = distance from the known faces. bigger confidence value = bigger "standard deviation"
                self.phony(identity, self.confidence_sum / (nbr_of_rec + 1))
                ret = True
            nbr_of_rec = 0
            self.confidence_sum = 0


        if window:
            cv2.imshow("Video", frame)
            # cv2.startWindowThread()
        if confidence < 200:
            nbr_of_rec += 1
        return ret, nbr_of_rec, peoples, identity

    def train(self):
        self.model = cv2.face.LBPHFaceRecognizer_create()
        self.model.train(np.asarray(self.images),
                         np.asarray(self.images_index))

    #        print "OK Train"

    def identity(self, image):
        [p_index, p_confidence] = self.model.predict(image)
        found_identity = self.identities[p_index]
        return found_identity, p_confidence

    #        print "OK acquire"

    @staticmethod
    def extract_and_resize(frame, x, y, w, h):
        cropped = cv2.getRectSubPix(frame, (w, h), (x + w / 2, y + h / 2))
        grayscale = cv2.cvtColor(cropped, cv2.COLOR_BGR2GRAY)
        clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
        grayscale  = clahe.apply(grayscale)
        resized = cv2.resize(grayscale, (IMAGE_SIZE, IMAGE_SIZE))
        #        print "OK extract"
        return resized

    def recognize(self):
        self.read_images()  # rename as collection??
        self.train()

    def read_images(self, sz=None):
        #   """Reads the images in a given folder, resizes images on the fly
        #    if size is given.
        # Args:
        #        path: Path to a folder with subfolders representing the subjects (persons).
        #        sz: A tuple with the size Resizes

        c = 0
        self.images = []
        self.images_index = []
        for dirname, dirnames, filenames in os.walk(self.image_path):
            for subdirname in dirnames:
                self.identities.append(subdirname)
                #                print self.identities, subdirname
                subject_path = os.path.join(dirname, subdirname)
                for filename in os.listdir(subject_path):
                    try:
                        im = cv2.imread(os.path.join(subject_path,
                                                     filename), cv2.IMREAD_GRAYSCALE)
                        if sz is not None:
                            im = cv2.resize(im, sz)
                        self.images.append(np.asarray(im, dtype=np.uint8))
                        self.images_index.append(c)
                    except IOError as e:
                        print("I/O error({0}): {1}".format(e.errno, e.strerror))
                        raise
                c += 1
                #        print "OK read"

    def phony(self, identity, confidence):
        logger = logging.getLogger('root')
        confidence = int(confidence)
        s = "Je suis avec " + str(identity) + " (confiance = " + str(confidence) + ")"
        logger.debug("identity = {0} (confidence = {1})".format(identity, confidence))
        self.dict_xmpp['to'] = 'muc'
        self.dict_xmpp['msg'] = s
        dict_lcd = {'msg':'Coucou ' + str(identity) + '\n' + str(confidence) , 'color':'blue', 'menu':' '}
        self.d_lcd = update_manager_dict(self.d_lcd, dict_lcd)

def update_manager_dict(input_manager_dict, new_dict):
        dict_temp = input_manager_dict
        for key, value in new_dict.items():
            dict_temp[key] = value
        input_manager_dict = dict_temp
        return input_manager_dict

