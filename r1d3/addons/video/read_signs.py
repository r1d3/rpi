#!/usr/bin/env python
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of R1D3.
#
# R1D3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# R1D3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with R1D3.  If not, see <http://www.gnu.org/licenses/>.
#


# Most of the code is inspired by the following pages:
# https://bitbucket.org/RoboBasics/raspberry-robo-cars/src/Scripts/reading_signs.py?at=master
# http://roboticssamy.blogspot.nl/

import numpy as np
import cv2
import logging


class Sign:
    def __init__(self, window, resw, resh, filename):
        self.logger = logging.getLogger('root')
        self.window = window
        self.filename = filename
        self.resw = resw
        self.resh = resh
        self.bot_sign = self.get_reference_images()
        self.max_mse_v = 19000
        self.min_mse_v = 15000000

    def get_reference_images(self):
        sign_bot = None
        try:
            sign_bot = cv2.imread(self.filename, 0)
            sign_bot = cv2.GaussianBlur(sign_bot, (5, 5), 0)
            _, sign_bot = cv2.threshold(sign_bot, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        except (OSError, IOError):
            print("ERROR FILE NOT FOUND")
            self.logger.error('File %d not found' % self.filename)

        return sign_bot

    @staticmethod
    def rgb2bgr(lst):
        a = []
        for i in reversed(lst):
            a.append(i)
        return a

    @staticmethod
    def annotation_color(image, centroid_x, centroid_y, camera_range, area):
        font = cv2.FONT_HERSHEY_SIMPLEX
        central = 'centre  %d : %d ' % (centroid_x, centroid_y)
        distance = 'range   %d ' % camera_range
        area = 'area   %d ' % area
        # (0, 255, 0) = green
        cv2.putText(image, central, (20, 25), font, 0.5, (0, 255, 0), thickness=2, lineType=cv2.LINE_AA, bottomLeftOrigin=0)
        cv2.putText(image, distance, (20, 45), font, 0.5, (0, 255, 0), thickness=2, lineType=cv2.LINE_AA, bottomLeftOrigin=0)
        cv2.putText(image, area, (20, 65), font, 0.5, (0, 255, 0), thickness=2, lineType=cv2.LINE_AA, bottomLeftOrigin=0)
        return image

    def annotation_roi(self, image, x2):
        font = cv2.FONT_HERSHEY_SIMPLEX
        # (0, 255, 0) = green
        x2 = "MSE = " + str(x2)
        cv2.putText(image, x2, (20, 85), font, 0.5, (125, 125, 255), thickness=2, lineType=cv2.LINE_AA, bottomLeftOrigin=0)
        cv2.putText(image, "MIN MSE " + str(self.min_mse_v), (20, 45), font, 0.5, (125, 125, 0), thickness=2, lineType=cv2.LINE_AA,
                    bottomLeftOrigin=0)
        return image

    @staticmethod
    def find_contours(result_image):
        contours, _ = cv2.findContours(result_image,
                                       cv2.RETR_LIST,
                                       cv2.CHAIN_APPROX_SIMPLE)
        # Find contours of all shapes
        return contours

    def color_tracking(self, frame):
        """
            frame : the current frame
            All we are doing here is defining a list of boundaries in the RGB color space
            (or rather, BGR, since OpenCV represents images as NumPy arrays in reverse order),
            where each entry in the list is a tuple with two values: a list of lower limits and a list of upper limits.
            For example, let's take a look at the tuple  ([17, 15, 100], [50, 56, 200]) .
            Here, we are saying that all pixels in our image that have a R >= 100, B >= 15, and G >= 17
            along with R <= 200, B <= 56, and G <= 50 will be considered red.
            Now that we have our list of boundaries, we can use the cv2.inRange function to perform the actual
            color detection.
        """

        # colors boundaries:
        # dark blue - light blue
        # brown
        # light blue too -- light green
        # grey/green -- grey

        # gaussian_img = cv2.GaussianBlur(frame, (9, 9), 2, 2)
        # ksize must be even for blur
        # blur_img = cv2.blur(frame,(10,10))
        # median: ksize must be odd and > 1
        blur_img = cv2.medianBlur(frame, 9)

        # cv2.imshow('gaussian', gaussian_img)
        # cv2.imshow('blur', blur_img)
        # cv2.imshow('median', median_img)


        boundaries = [
            ([10, 114, 120], [240, 201, 196]),
            ([96, 62, 88], [185, 123, 106]),
            ([106, 90, 57], [191, 206, 91]),
            ([25, 146, 190], [62, 174, 250]),
            ([103, 86, 65], [145, 133, 128]),
            ([79, 0, 0], [255, 255, 255])
        ]  # actual color detection:
        # 0: red
        # 1 : blue
        # 2 : green
        # 3 : orange
        # 4 : grey
        # 5 : test
        a, b = boundaries[0]
        col_lo = self.rgb2bgr(a)
        col_high = self.rgb2bgr(b)
        hsv = cv2.cvtColor(blur_img, cv2.COLOR_BGR2HSV)
        lower = np.array(col_lo, dtype="uint8")
        upper = np.array(col_high, dtype="uint8")
        # find the colors within the specified boundaries and apply
        # the mask
        mask = cv2.inRange(hsv, lower, upper)
        # After calling cv2.inRange, a binary mask is returned, where white pixels (255) represent pixels that fall
        # into the upper and lower limit range and black pixels (0) do not.
        output = cv2.bitwise_and(frame, frame, mask=mask)

        # Possible enhancement: detect blue then red: 2 colors on the sign ?
        # Convert to Gray (needed for binarizing)
        output_grey = cv2.cvtColor(output, cv2.COLOR_BGR2GRAY)
        # find contour of the output and see the area. if the area is big enough (> 2000), track the sign.
        contours = self.find_contours(output_grey)
        # Select biggest; drop the rest
        contours_sorted = sorted(contours, key=cv2.contourArea, reverse=True)[:1]
        ret = False
        max_area = 2000

        frame_roi = frame
        frame_display = None
        if len(contours_sorted) > 0:
            cnt = contours_sorted[0]
            x, y, w, h = cv2.boundingRect(cnt)
            centroid_x = x + (w / 2)
            centroid_y = y + (h / 2)
            # real width * focal length = 16175.288
            # todo wtf ?????
            # Logitech C170 Focal lenght = 2.3 mm
            camera_range = round((16175.288 / w), 0)
            # Not needed; Just to display the difference
            # cv2.drawContours(frame, [cnt], -1, (0, 0, 255), 2)
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
            area = w * h
            if centroid_x != self.resw and centroid_y != self.resw:
                frame_display = self.annotation_color(frame, centroid_x, centroid_y, camera_range, area)
            if area > max_area:
                ret = True
                frame_roi = frame[y:y + h, x:x + w]

        if self.window:
            #cv2.imshow('mask', mask)
            #cv2.imshow('Color Detected', output)
            #cv2.imshow('BOT', self.bot_sign)
            try:
                if frame_display.any():
                    cv2.imshow('Input', frame_display)
            except AttributeError:
                pass

        return ret, frame_roi

    def sign_tracking(self, frame):
        # result_image = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        #        result_image = cv2.bilateralFilter(frame,9,75,75)
        # Find edges of all shapes
        # result_image = cv2.bilateralFilter(frame,9,75,75)
        result_image = cv2.Canny(frame, threshold1=90, threshold2=190)
        contours = self.find_contours(result_image)
        # Select biggest 4; drop the rest
        contours = sorted(contours, key=cv2.contourArea, reverse=True)[:4]
        objects_found = len(contours)
        area_save = 0
        contour_saved = None
        sign_filtered = False
        if objects_found >= 1:
            loop_count = 0
            rectangle_count = 0
            while loop_count < objects_found:
                cnt = contours[loop_count]
                # moments = cv2.moments(cnt)
                area = cv2.contourArea(cnt)
                epsilon = 0.1 * cv2.arcLength(cnt, True)
                approx = cv2.approxPolyDP(cnt, epsilon, True)
                if len(approx) == 4:
                    if area > 5000:
                        if loop_count == 0:  # 8 - Keep the smallest aledged rectangle
                            rectangle_count += 1
                            contour_saved = approx
                            area_save = area
                        elif area < area_save:
                            rectangle_count += 1
                            contour_saved = approx
                loop_count += 1
            if rectangle_count > 0:
                sign_filtered = True

        mse_v = 1000000000
        if sign_filtered:
            mse_v = self.compare_images(frame, contour_saved)

        if self.window:
            cv2.imshow('sign_tracking', result_image)
            cv2.imshow('ROI', frame)

        return sign_filtered, mse_v

    def compare_images(self, frame, contour):
        contour_points = contour.reshape(4, 2)
        # initializing output window in same order
        points_sorted = np.zeros((4, 2), dtype="float32")
        # determine top-left, top-right, bottom-right, bottom-left
        sum_of_points = contour_points.sum(axis=1)
        points_sorted[0] = contour_points[np.argmin(sum_of_points)]
        points_sorted[2] = contour_points[np.argmax(sum_of_points)]
        difference = np.diff(contour_points, axis=1)
        points_sorted[1] = contour_points[np.argmin(difference)]
        points_sorted[3] = contour_points[np.argmax(difference)]
        (tl, tr, br, bl) = points_sorted
        # need this; for example: it could as well be a parallelogram
        width_a = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[0] - bl[0]) ** 2))
        width_b = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[0] - tl[0]) ** 2))
        height_a = np.sqrt(((tr[1] - br[1]) ** 2) + ((tr[1] - br[1]) ** 2))
        height_b = np.sqrt(((tl[1] - bl[1]) ** 2) + ((tl[1] - bl[1]) ** 2))
        max_width = max(int(width_a), int(width_b))
        max_height = max(int(height_a), int(height_b))
        destination = np.array([[0, 0], [max_width - 2, 0],
                                [max_width - 2, max_height - 2],
                                [0, max_height - 2]], dtype="float32")
        perspective_transform = cv2.getPerspectiveTransform(points_sorted, destination)
        warped_image = cv2.warpPerspective(frame, perspective_transform, (max_width, max_height))
        cv2.drawContours(frame, [contour], -1, (255, 0, 0), 2)

        # convert to gray, blur and threshold
        warped_image = cv2.cvtColor(warped_image, cv2.COLOR_BGR2GRAY)
        warped_image = cv2.GaussianBlur(warped_image, (5, 5), 0)
        thrh, warped_image = cv2.threshold(warped_image, 0, 255,
                                           cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        # equalize shapes
        (hc, wc) = self.bot_sign.shape[:2]
        resized = cv2.resize(warped_image, (wc, hc),
                             interpolation=cv2.INTER_AREA)
        cv2.waitKey(1)
        # create window for bitwise output
        mse_v = self.mse(resized, self.bot_sign)
        if self.window:
            cv2.imshow("Cnt Found", frame)
            resized = self.annotation_roi(resized, mse_v)
            cv2.imshow('resized', resized)
        if mse_v < self.max_mse_v:
            s = "Sign Detected: " + str(mse_v)
            self.logger.info(s)
        if mse_v < self.min_mse_v:
            self.min_mse_v = mse_v
        return mse_v

    @staticmethod
    def mse(image_a, image_b):

        """
        Mean Squared Error = differance in pixel intensity
        :param image_a: first image
        :param image_b: second image
        :return:the difference (chi square) between the two images
        """

        err = np.sum((image_a.astype("float") - image_b.astype("float")) ** 2)
        err /= float(image_a.shape[0] * image_a.shape[1])
        return err

    def sign_reading_process(self, frame):
        sign_found = False
        ret, frame_roi = self.color_tracking(frame)
        mse_v = 1000000000
        if ret:
            ret, mse_v = self.sign_tracking(frame_roi)
        if mse_v < self.max_mse_v:
            sign_found = True
            # analyse size of the sign, direction, to control motors
        return sign_found
