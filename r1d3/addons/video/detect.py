#!/usr/bin/env python
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of R1D3.
#
# R1D3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# R1D3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with R1D3.  If not, see <http://www.gnu.org/licenses/>.
#

import cv2
import logging
import time
import numpy as np
import random
import datetime
import os
from . import tracking as t
from . import recognize
from . import read_signs

logger = logging.getLogger('root')


# http://stackoverflow.com/questions/23100704/running-infinite-loops-using-threads-in-python

class Detection:
    def __init__(self, inter, win, d_detect, d_mode, d_xmpp, d_lcd, colors, mode_skeleton):

        self.interval = inter
        self.window = win
        self.d_detect = d_detect
        self.d_mode = d_mode
        self.d_xmpp = d_xmpp
        self.d_lcd = d_lcd
        self.colors = colors
        self.resh = 0
        self.resw = 0
        self.Brightness = 0
        self.Contrast = 0
        self.minNeighbors = 7  # 10 may be slower
        self.traget_detected = 0
        self.prevac = 0
        self.checkpoint = 0
        self.facedetect = 0
        self.track_object = False
        self.selection = (0, 0, 0, 0)
        self.scale = 0
        self.logger = None
        self.movie = False
        self.movie_path = False
        self.mode_skeleton = mode_skeleton

    def change_mode(self):
        if self.traget_detected == 0:
            self.traget_detected = 1
            emotion("Seek mode, we have something to seek :-)")
        return self.traget_detected

    def run(self, value):
        self.d_detect['frame_capture'] = value
        self.d_mode['frame_capture'] = value
        self.d_mode['video_mode'] = value

    def start_session(self, camera, cascpath, faces_path, movie, movie_path):
        self.movie = movie
        self.movie_path = movie_path
        obj_cascade = []
        for f in cascpath:
            obj_cascade.append([cv2.CascadeClassifier(f), f])
        # ["lbpcascade_frontalface.xml"] :
        face_trainset = [x[1] for x in obj_cascade if "lbpcascade_frontalface" in x[1]]
        try:
            face_trainset = face_trainset[0]  # I need the string
        except IndexError:
            msg = "no trainset availaible here: {}".format(cascpath)
            logging.critical("Empty trainset ".format(face_trainset))
            logging.critical(msg)
            return None
        video_capture = cv2.VideoCapture(camera)  # camera may be the argument of capture
        if video_capture.isOpened():  # check if we succeeded
            # video_capture.set(cv2.CAP_PROP_FRAME_HEIGHT, self.resh)
            # video_capture.set(cv2.CAP_PROP_FRAME_WIDTH, self.resw)
            # other parameters http://stackoverflow.com/questions/11420748/setting-camera-parameters-in-opencv-python
            # video_capture.set(cv2.cv.CV_CAP_PROP_BRIGHTNESS, self.Brightness)
            # video_capture.set(cv2.cv.CV_CAP_PROP_CONTRAST, self.Contrast)
            self.run(True)
            self.main_loop(obj_cascade, video_capture, faces_path, face_trainset)

    def cam_parameters(self, resw, resh, brightness=1, contrast=1, traget_detected=0,
                       checkpoint=0, prevac=0, facedetect=0, scale=1):
        self.Brightness = brightness
        self.Contrast = contrast
        self.resw = resw
        self.resh = resh
        self.traget_detected = traget_detected
        self.prevac = prevac
        self.checkpoint = checkpoint
        self.facedetect = facedetect
        self.scale = scale

    def detection2(self, frame, cascade, color):
        minisize = (frame.shape[1] / self.scale, frame.shape[0] / self.scale)
        miniframe = cv2.resize(frame, minisize)
        obj = cascade.detectMultiScale(miniframe)
        results = [0, 0, 0, 0, 0, 0]
        # sc = 1
        for o in obj:
            x, y, w, h = [v * self.scale for v in o]
            ac = w * h
            # pt1 = (x + w / 2, y + h / 2)
            # pt2 = (x, y)
            if w != 0 and h != 0:
                cv2.rectangle(frame, (x, y), (x + w, y + h), color, 2)  # 2 because negative values = filled
                results = [x, w, y, h, ac, 1]  # 1 because ?? TODO

        return results

    def detection(self, frame, cascade, color):

        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
        gray = clahe.apply(gray)
        gray = np.array(gray, dtype='uint8')
        in_frame = gray

        obj = cascade.detectMultiScale(
            in_frame,
            scaleFactor=1.1,  # 1.05 slower, 1.4 faster but results not as good
            minNeighbors=self.minNeighbors,
            #            flags=cv2.cv.CV_HAAR_FIND_BIGGEST_OBJECT,
            minSize=(30, 30),
            #  flags=cv2.cv.CV_HAAR_SCALE_IMAGE
        )
        # calculate the area of the fist/face

        # Draw a rectangle around the faces
        results = [0, 0, 0, 0, 0, 0]
        # sc = 1
        for o in obj:
            x, y, w, h = [v * self.scale for v in o]
            ac = w * h
            #  pt1 = (x + w / 2, y + h / 2)
            # pt2 = (x, y)
            if w != 0 and h != 0:
                cv2.rectangle(frame, (x, y), (x + w, y + h), color, 2)  # 2 because negative values = filled
                results = [x, w, y, h, ac, 1]  # 1 because ?? TODO

        return results

    def detect_draw(self, t1, frame, obj_cascade):

        obj_name = ' '
        colors = [self.colors['red'], self.colors['green'], self.colors['blue'],
                  self.colors['orange'], self.colors['yellow']]
        color = random.choice(colors)
        res = [0, 0, 0, 0, 0, 0]
        # TODO test with this value of self.minneightbour if the program is not too slow on the PI
        for cascade in obj_cascade:
            res = self.detection(frame, cascade[0], color)
            if res[4] > 0:
                # first detected object = OK
                obj_name = cascade[1]
                break

        if obj_name != ' ':
            logger.debug("Object detected : " + obj_name)
        ac = res[4]
        centerx = res[1] / 2 + res[0]
        centery = res[3] / 2 + res[2]
        x = res[0]
        w = res[1]
        y = res[2]
        h = res[3]

        self.selection = (x, x + h, y, y + w)
        color = (0, 255, 0, 0)
        cv2.circle(frame, (int(round(centerx)), int(round(centery))), 3, color, 2, 8, 0)  # green circle
        t2 = time.time()
        delta = t2 - t1

        if 25 < delta < 45 and self.checkpoint == 0 and self.traget_detected == 1:
            logger.info("25 < delta =  < 45")  # TODO: add checkpoint, delta formatting
            self.checkpoint = 1
            logger.info("No detection for 25 s")
            emotion("emotion")
        elif 45 < delta < 100 and self.checkpoint == 1 and self.traget_detected == 1:
            logger.info("no face for " + str(delta) + " seconds. Have to scan around to search people.")
            self.checkpoint = 2
            emotion("direction= arduino")
            self.traget_detected = 0

        elif delta > 100 and self.checkpoint == 2:
            self.checkpoint = 3
            logger.info("checkpoint > 3 and delta > 100")
            self.traget_detected = 0
            self.facedetect = 0
            emotion("Grumpy, go to exploration mode")
            logger.info("Grumpy, go to exploration mode")

        if ac != 0 and centerx != 0 and centery != 0:
            logger.debug("We have some objects !")
            self.checkpoint = 0
            if self.facedetect > 0 or self.traget_detected == 0:
                self.facedetect = 0
                emotion("We have something and maybe we will enter the tracking mode")
                logger.debug("We have something and maybe we will enter the tracking mode")
            self.facedetect += 1  # now we have something to detect
            if self.traget_detected == 0:
                logger.info("Facedetect = " + str(self.facedetect))
                roger = serialevent()
                if roger == 1:
                    logger.debug("Got the object and Arduino is OK to follow it")
                    self.traget_detected = self.change_mode()
            logger.debug(str(obj_name) + " Area: " + str(ac) + " x = " + str(x + w / 2) + " y = " + str(y + h / 2))
            t1 = time.time()
            self.track_object = True

        # todo brain(res, frame.cols/2, frame.row/2, detect)

        # Display the resulting frame
        if self.window:
            cv2.imshow('Video', frame)

        return [t1, obj_name, res]

    def main_loop(self, obj_cascade, video_capture, faces_path, face_trainset):
        logger.debug('In capture...')
        t1 = time.time()
        ser = 0
        hound = t.Tracking(ser, self.window)
        cascade_name = " "
        max_nbr_of_rec = 10
        r = recognize.Recognize(faces_path, face_trainset, max_nbr_of_rec, self.d_lcd)
        filename = faces_path + '/robot.png'
        rs = read_signs.Sign(self.window, self.resw, self.resh, filename)
        peoples = []
        nbr_of_rec = 0
        r.recognize()
        obj_box = [0, 0, 0, 0]
        dico_temp_xmpp = {'to': ' ', 'msg': ' ', 'status': ' ', 'status_msg': ' ', 'roger': '1', 'send_dict': '0'}
        # Warning, the FOURCC codec is linked to the filename.
        # fourcc value for avi file with logitech C170 camera is compatible with an AVI file
        #fourcc = cv2.VideoWriter_fourcc(*'XVID')
        codec = 0x47504A4D  # <-- Magic number corresponding to a MJPG Stream
        video_capture.set(cv2.CAP_PROP_FOURCC, codec)  # <-- You actually says that your camera produces such a stream
        video_capture.set(cv2.CAP_PROP_FRAME_WIDTH,
                640)  # <-- We fix the resolution, the framerate and use exactly the same in the VideoWriter arguments
        video_capture.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)
        video_capture.set(cv2.CAP_PROP_FPS, 20.0)  # <-- the framerate is not limitating here

        fourcc = cv2.VideoWriter_fourcc(*'MJPG')
        now = datetime.datetime.now().strftime("%Y-%m-%d-%HH%M")
        videoname = os.path.join(self.movie_path, f"output-{now}.avi")
        video_writer = cv2.VideoWriter(videoname, fourcc, 20.0, (640, 480))
        while self.d_mode['frame_capture']:
            # Capture frame-by-frame
            cascade_name = " "
            ret, frame = video_capture.read()
            # TODO BUG in here, the tracking procedure needs to be enhanced.
            if self.track_object:
                # tracking seek_and_destroy
                # TODO BUG: Not working because the colors change at each frame. Keep the color of the original frame
                logger.debug("tracking mode")
                res = hound.seek_and_destroy(frame, self.track_object, self.selection, self.scale)
                t2 = res[0]
                self.selection = res[1]
                self.track_object = res[2]
                if t2 > 0:
                    t1 = t2

            if self.d_mode['sign_tracking']:
                ret = rs.sign_reading_process(frame)
                if ret and self.d_mode['sign_phony'] == False:
                    s = "J'ai trouvé le signe du robot!"
                    dico_temp_xmpp['to'] = 'muc'
                    dico_temp_xmpp['msg'] = s
                    update_manager_dict(self.d_xmpp, dico_temp_xmpp)
                    mode_skeleton_new = self.mode_skeleton
                    mode_skeleton_new['sign_phony'] = True
                    update_manager_dict(self.d_mode, mode_skeleton_new)
                    dict_lcd = {'msg': "J'ai trouve le" + '\n' + "signe du robot!", 'color': 'blue', 'menu': ' '}
                    update_manager_dict(self.d_lcd, dict_lcd)

            if self.d_mode['facedetect_mode']:
                # t = time.time()
                res = self.detect_draw(t1, frame, obj_cascade)
                t1 = res[0]
                cascade_name = res[1]
                obj_box = res[2]

            if "face" in cascade_name:  # if a new person is detected, talk about it
                # detection of a face: let's identify!
                # then talk about it on xmpp
                ret, nbr_of_rec, peoples, identity = r.frame_process(frame, obj_box, self.window, nbr_of_rec,
                                                                     peoples)
                if ret:
                    dico_temp_xmpp = r.update_d_xmpp()
                    update_manager_dict(self.d_xmpp, dico_temp_xmpp)

            if self.movie:
                video_writer.write(frame)

            time.sleep(self.interval)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        # When everything is done, release the capture

        if self.window:
            cv2.waitKey(1)
            cv2.destroyAllWindows()
            logger.debug('destroy Video window')
        video_capture.release()


def update_manager_dict(input_manager_dict, new_dict):
    dict_temp = input_manager_dict
    for key, value in new_dict.items():
        dict_temp[key] = value
    input_manager_dict = dict_temp
    return input_manager_dict


def emotion(s):
    # TODO: sing something or go in a direction (follow the object cf Beaglebone Black)
    logger.debug(str(s))
    return 0


def serialevent():
    logger.debug("get the roger message from the arduino")
    return 1


def init_detection(interval, window, d_mode, d_detect, d_xmpp, d_lcd, resh, resw, traget_detected,
                   checkpoint, prevac, facedetect, scale, camera_num, cascpath,
                   faces_path, movie, movie_path, colors, mode_skeleton):
    ru = Detection(interval, window, d_detect, d_mode, d_xmpp, d_lcd, colors, mode_skeleton)
    ru.cam_parameters(resh, resw, 1, 1, traget_detected, checkpoint, prevac, facedetect, scale)
    ru.start_session(camera_num, cascpath, faces_path, movie, movie_path)

    # ru = None


# def stop_detection():
# ru.run(False)


if __name__ == '__main__':
    interval = 0.1
    camera_num = 0
    window = True
    cascade_dir = "../../../cascades/" + 'fist.xml'
    cascade_dir2 = "../../../cascades/" + 'lbpcascade_frontalface.xml'
    sc = 2.0
    resH = 768
    resW = 1024
# r = Detection(interval, window, sc)
#    r.cam_parameters(resH, resW)
#    r.start_session(camera_num, cascade_dir)
