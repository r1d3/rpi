#!/usr/bin/env python
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of R1D3.
#
# R1D3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# R1D3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with R1D3.  If not, see <http://www.gnu.org/licenses/>.
#

import logging
# , categorize, ecodes
from evdev import InputDevice, ecodes, categorize
import selectors
import ipdb
import os
from time import sleep, time

# from time import sleep

selector = selectors.DefaultSelector()

logger = logging.getLogger('root')

GAMEPAD_FILE = "/dev/input/event3"

# FIXME when start and remote controller is not paired
# FileNotFoundError: [Errno 2] No such file or directory: '/dev/input/event3'

BUTTONS = [
    {'code': 17, 'value': -1, 'name': 'up', 'symbol': '↑', 'serial': {}, 'action': 'up', 'direction': None},
    {'code': 17, 'value': 1, 'name': 'down', 'symbol': '↓', 'serial': {}, 'action': 'down', 'direction': None},
    {'code': 16, 'value': 1, 'name': 'right', 'symbol': '→', 'serial': {'serial': True, 'data': 'r=3'},
     'action': 'right', 'direction': None},
    {'code': 16, 'value': -1, 'name': 'left', 'symbol': '←', 'serial':
        {'serial': True, 'data': 'r=4', 'desc': 'direction'}, 'action': 'left', 'direction': None},
    {'code': 304, 'value': 1, 'name': 'cross', 'symbol': 'X', 'serial':
        {'serial': True, 'data': 'r=1', 'desc': 'direction', 'direction': 1},
     'action': 'forward', 'direction': None},
    {'code': 305, 'value': 1, 'name': 'circle', 'symbol': '○',
     'serial': {'serial': True, 'data': 'r=2', 'desc': 'direction'}, 'action': 'backward', 'direction': -1},
    {'code': 307, 'value': 1, 'name': 'triangle', 'symbol': '△',
     'serial': {'serial': True, 'data': 'r=5', 'desc': 'direction'}, 'action': 'stop', 'direction': None},
    {'code': 308, 'value': 1, 'name': 'square', 'symbol': '□',
     'serial': {'serial': True, 'data': 'l=1', 'desc': 'Scan Lidar'}, 'action': 'Lidar detect obstacles',
     'direction': None},
    {'code': 310, 'value': 1, 'name': 'L1', 'symbol': None, 'serial': {'serial': True, 'data': 's=0', 'desc': 'speed'},
     'action': 'max speed down', 'direction': None},
    {'code': 311, 'value': 1, 'name': 'R1', 'symbol': None, 'serial': {'serial': True, 'data': 's=1', 'desc': 'speed'},
     'action': 'max speed up', 'direction': None},
    {'code': 312, 'value': 1, 'name': 'L2', 'symbol': None, 'serial':
        {'serial': True, 'data': 'a=2', 'desc': 'small angle left'}, 'action': 'small angle left', 'direction': None},
    {'code': 313, 'value': 1, 'name': 'R2', 'symbol': None, 'serial':
        {'serial': True, 'data': 'a=1', 'desc': 'small angle right'}, 'action': 'small angle right', 'direction': None},
    {'code': 314, 'value': 1, 'name': 'share', 'symbol': None,
     'serial': {'serial': True, 'data': 'mm=0', 'desc': 'mode'}, 'action': 'mode -', 'direction': None},
    {'code': 315, 'value': 1, 'name': 'options', 'symbol': None,
     'serial': {'serial': True, 'data': 'mm=1', 'desc': 'mode'}, 'action': 'mode +', 'direction': None},
    {'code': 316, 'value': 1, 'name': 'playsation', 'symbol': None, 'serial': {}, 'action': '', 'direction': None},
    {'code': 317, 'value': 1, 'name': 'analog_left', 'symbol': None, 'serial': {}, 'action': '', 'direction': None},
    {'code': 318, 'value': 1, 'name': 'analog_right', 'symbol': None, 'serial': {}, 'action': '', 'direction': None},
]


class BluetoothListener:
    def __init__(self, d_mode, d_lcd, d_ser, mode_skeleton, ):
        logger.info("Bluetooth listener")
        self.d_mode = d_mode
        self.d_lcd = d_lcd
        self.d_ser = d_ser
        self.mode_skeleton = mode_skeleton
        self.ps4_timer = millis()
        # do not send more than 8 commands per second
        self.threashold_timer = 125
        self.direction = None

        # creates object 'gamepad' to store the data
        # you can call it whatever you like
        # self.trackpad = InputDevice('/dev/input/event0')
        # motion sensor is 2 but it's too complicated for now.

    def listen(self):
        """
        Loop to listen to the buttons.
        When I want to use analog buttons and the trackpad, I will have to use:
        while True:
            for key, mask in selector.select():
                device = key.fileobj
                for event in device.read():
                    send = False
                    dict_ser = {}
                    etc
        :return:
        """
        gamepad = InputDevice(GAMEPAD_FILE)
        good_codes = [x['code'] for x in BUTTONS]
        for event in gamepad.read_loop():
            code = int(event.code)
            if code in good_codes:
                value = int(event.value)
                key_event = categorize(event)
                if value == 0:
                    # it's like triangle:
                    code = 307
                    value = 1
                action = [x for x in BUTTONS if x['code'] == code and x['value'] == value]
                if action:
                    action = action[0]
                    # We only permit to turn if X or 0 are pressed
                    # TODO check if the problems comes back
                    # if not self.direction and action.get('name') in ('right', 'left'):
                    #     # We cannot turn. This is to prevent phantom turns with micro pressures on the keypad
                    #     continue
                    desc = action['action']
                    log_msg = f"Code {code}, Value {value}, {desc}"
                    logger.info(log_msg)
                    dict_ser = action.get('serial')
                    if dict_ser and millis() - self.ps4_timer > self.threashold_timer:
                        logger.info("Bluetooh: sent serial : " + str(dict_ser))
                        self.d_ser = update_manager_dict(self.d_ser, dict_ser)
                        self.ps4_timer = millis()
                        self.direction = action.get("direction")


def millis():
    return int(round(time() * 1000))


def init_bluetooth(d_mode, d_lcd, d_ser, mode_skeleton):
    ret = None
    while not ret:
        sleep(0.5)
        ret = os.path.exists(GAMEPAD_FILE)
    bluetooth_instance = BluetoothListener(d_mode, d_lcd, d_ser, mode_skeleton)
    bluetooth_instance.listen()


def update_manager_dict(input_manager_dict, new_dict):
    dict_temp = input_manager_dict
    for key, value in new_dict.items():
        dict_temp[key] = value
    input_manager_dict = dict_temp
    return input_manager_dict
