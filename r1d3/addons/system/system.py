#!/usr/bin/env python3

##!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
# @author: Arnaud Joset
#
# This file is part of R1D3.
#
# R1D3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# R1D3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with R1D3.  If not, see <http://www.gnu.org/licenses/>.

# This file is based on rpslam.py : BreezySLAM Python with SLAMTECH RP A1 Lidar of Simon D. Levy.
#
"""

import logging
from time import sleep
from subprocess import Popen, PIPE
# from systemd import journal

logger = logging.getLogger('root')


class System:
    def __init__(self, d_ser, mode_skeleton, d_mode, d_xmpp, d_lcd, system_options):
        logger.info("Starting periodic system checks...")
        self.d_mode = d_mode
        self.d_ser = d_ser
        self.mode_skeleton = mode_skeleton
        self.d_xmpp = d_xmpp
        self.d_lcd = d_lcd
        self.battery_pattern = system_options.get('battery_pattern')
        self.battery_threashold= system_options.get('battery_threashold')
        self.battery_events = []

    def run(self):
        while True:
            if self.battery_pattern and self.battery_threashold:
                result = self.check_battery_state()
                if len(self.battery_events) > self.battery_threashold:
                    logging.critical("Battery Low !!")
            sleep(5)

    def check_battery_state(self):
        # journal_instance = journal.Reader()
        # journal_instance.this_boot()
        # journal_instance.enumerate_fields()
        # arj fixme test
        # journal_instance.add_match(_SYSTEMD_UNIT='systemd-networkd')
        # for entry in journal_instance:
        for entry in [None]:
            if (self.battery_pattern in entry['MESSAGE']) and entry not in self.battery_events:
                self.battery_events.append(entry)
                print("MESSAGE SYSTEM : ", entry['MESSAGE'])
                # todo add granularity: time since last message, a lot of message in a few time,
                # todo some messages then nothing for 30 min etc


def init_system(d_xmpp, d_mode, d_lcd, d_ser, mode_skeleton, d_socket, system_options):
    system_instance = System(d_ser=d_ser, mode_skeleton=mode_skeleton, d_mode=d_mode,
                             d_xmpp=d_xmpp, d_lcd=d_lcd, system_options=system_options)
    system_instance.run()
