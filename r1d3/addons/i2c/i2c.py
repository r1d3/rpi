#!/usr/bin/env python
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of R1D3.
#
# R1D3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# R1D3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with R1D3.  If not, see <http://www.gnu.org/licenses/>.
#

import time
try:
    import Adafruit_GPIO.I2C as I2C
    import RTIMU
except ModuleNotFoundError:
    from . import dummy_i2c as I2C
    from . import dummy_rtimu as RTIMU
import logging

import math

logging.basicConfig()
logger = logging.getLogger(__name__)


# // EXPLANATIONS:
# // ReceiveEvent: Raspberry PI ask to do something
# // The Raspberry PI requests an action from the Arduino. It send a register and a value:
# //
# // i2c.write8(0x00,0x01)
# //
# // Register    Description
# // 0x00        Execution register.
# //            Place one of the accepted values here and the Arduino will execute your request.
# //            This register can also be read.
# // 0x01       Measure distance with ultrasonic sensors
# // 0x02       Measure the data from the MiniMu-9
# // 0x03       Measure the data from the MiniMu-9 (the sequel)
# //
# // Register 0x00: The accepted values are:
# // Value 0x00  : Do nothing
# // Value 0x01  : Send the value of distance measurement when asked.
# // Value 0x02  : Send the value of MiniMU when asked.
# // Value 0x03  : Send the value of MiniMU (sequel) when asked.
# // On Arduino, thenbuffer lenght is equal to 32 bytes. It prevent it to send all the data at once.
#
#
# // requestEvent: Raspberry PI ask the result of the what it asked.
# // print(i2c.readU8(0x00))
# // Arduino read the last executed register and gives the result accordinately:
# //

class Talky:
    def __init__(self, arduino, settings_imu):
        self.arduino = arduino
        bus_version = 1
        self.i2c = I2C.Device(self.arduino, bus_version)
        self.i2c_sleep_time = 0.15
        self.s = RTIMU.Settings(settings_imu)
        self.imu = RTIMU.RTIMU(self.s)
        logger.info("IMU Name: " + self.imu.IMUName())
        if (not self.imu.IMUInit()):
            logger.error("IMU Init Failed")
        else:
            print("IMU Init Succeeded")
            self.imu.setSlerpPower(0.02)
            self.imu.setGyroEnable(True)
            self.imu.setAccelEnable(True)
            self.imu.setCompassEnable(True)
            self.poll_interval = self.imu.IMUGetPollInterval()

    def get_distances(self):
        dict_distances = {}
        try:
            self.i2c.write8(0, 0x01)  # Place 1 in register 0.
            time.sleep(self.i2c_sleep_time)
            dist_raw = self.i2c.readList(0x00, 4)
            time.sleep(self.i2c_sleep_time)
            #logger.warning(dist_raw)
            # print("Dist Raw : ", dist_raw)
            # Raw data are byte arrays. We replace the null bytes by null caracter "" and then convert
            # the byte array to utf-8 string
            #            dist_raw = dist_raw.replace('\xff', "")
            #            dist_raw = dist_raw.decode('utf-8')
            # python3 : dist_raw = str(dist_raw, 'utf-8')
            #            dist_raw = str(dist_raw)
            dict_distances = self.process_distances(dist_raw)
        except IOError:
            logger.error("I2C IOerror")
            pass
        return dict_distances

    def process_distances(self, raw_data):
        # "d front, d back, d left, d right"
        dict_distances = {}
        i = 0
        for d in ("front", "back", "left", "right"):
            try:
                # print("index " + i)
                # print("raw data " + raw_data[i])
                dict_distances[d] = raw_data[i]
            except TypeError:
                msg = "Type : " + str(i) + " " + str(raw_data[i])
                logger.error(msg)
            i += 1
        return dict_distances

    def get_minimu(self):
        """
        Example data:
        {'pressureValid': False, 'accelValid': True, 'temperature': 907401101312.0, 'pressure': 204.2314910888672,
        'fusionQPoseValid': True, 'timestamp': 1481988150250131L, 'compassValid': True,
        'compass': (-4.78645133972168, -15.981008529663086, -18.588661193847656),
        'accel': (0.15615999698638916, -0.21862399578094482, -0.9057279825210571),
        'humidity': 3397.953857421875, 'gyroValid': True,
        'gyro': (-0.03351954370737076, -0.018711263313889503, 0.003365498036146164),
        'temperatureValid': False, 'humidityValid': False,
        'fusionQPose': (-0.02759035862982273, -0.44636499881744385, 0.891329288482666, -0.07435896247625351),
        'fusionPoseValid': True, 'fusionPose': (-3.032723903656006, -0.11582546681165695, -2.2193589210510254)}

        :return:
        """
        dict_sensors = {}
        if self.imu.IMURead():
            data = self.imu.getIMUData()
            fusionPose = data["fusionPose"]
            time.sleep(self.poll_interval * 1.0 / 1000.0)
            dict_sensors['euler'] = [math.degrees(fusionPose[0]), math.degrees(fusionPose[1]),
                                     math.degrees(fusionPose[2])]
            dict_sensors['time'] = data['timestamp']
            compass = data['compass']
            dict_sensors['compass'] = [compass[0], compass[1], compass[2]]
            acceleration = data['accel']
            dict_sensors['acceleration'] = [acceleration[0], acceleration[1], acceleration[2]]
        return dict_sensors

    def get_minimu_old(self):
        """
        Get the i2c data from Arduino
        3 set of data are available:
        Euler values:     E|-22.04;52.26;-33.36%ÿÿÿÿÿÿÿÿ
        Analog values:    A|-33;4;2;-13;-5;-241%ÿÿÿÿÿÿÿÿ
        magnetic values:  B|1.16;0.42;2.26%ÿÿÿÿÿÿÿÿÿÿÿÿÿ
        :return:
        """

        dict_sensors = {}
        try:
            reg = [0x2, 0x3, 0x4]
            minimu_data = []
            for register in reg:
                self.i2c.write8(0, register)  # Place 2 in register.
                time.sleep(self.i2c_sleep_time)
                data = self.i2c.readList(0x00, 30)
                #logger.warning(data)
                data = data.replace('\xff', "")
                # python3 : dist_raw = str(dist_raw, 'utf-8')
                data.decode("utf-8")
                data = str(data)
                minimu_data.append(data)
                time.sleep(self.i2c_sleep_time)

            dict_sensors = self.process_minimu(minimu_data)

        except IOError:
            logger.error("I2C IOerror")
            pass
        return dict_sensors

    def process_minimu_old(self, minimu_data):
        """
            Process the minimu_data
            Euler values:     E|-22.04;52.26;-33.36%ÿÿÿÿÿÿÿÿ
            Analog values:    A|-33;4;2;-13;-5;-241%ÿÿÿÿÿÿÿÿ
            magnetic values:  B|1.16;0.42;2.26%ÿÿÿÿÿÿÿÿÿÿÿÿÿ
            :param minimu_data: list of the data buffers
            :return: a dictionnary containing the data
        """

        dict_sensors = {'euler': self.process_euler(minimu_data[0], "E"),
                        'analog': self.process_analog(minimu_data[1], "A"),
                        'magnetic': self.process_magnetic(minimu_data[2], "B")}
        # 0 : Euler
        # 1 : Analog
        # 3 : magnetic
        return dict_sensors

    def process_data(self, data, cat):
        """
            process Analog values
            :param cat: category
            :param data: sensor values
            :return:
        """
        sensor = data.split("%")
        dict_sensor_string = {}

        try:
            measurements = sensor[0]
            data_s = measurements.split(";")
            dict_sensor_string[cat] = [x for x in data_s]
        except IndexError:
            pass
        dict_sensor = {}
        for category, measurements in dict_sensor_string.items():
            print(category, measurements)
            list_values = []
            for m in measurements:
                try:
                    d = float(m)
                    list_values += [d]
                except ValueError:
                    pass
            dict_sensor[category] = list_values
        return dict_sensor

    def process_analog(self, analog_data, cat):
        dict_analog = self.process_data(analog_data, cat)
        # dict_analog['analog'] = dict_analog.pop('A')
        return dict_analog[cat]

    def process_magnetic(self, magnetic_data, cat):
        dict_magnetic = self.process_data(magnetic_data, cat)
        # dict_magnetic['magnetic'] = dict_analog.pop('B')
        return dict_magnetic[cat]

    def process_euler(self, euler_data, cat):
        dict_euler = self.process_data(euler_data, cat)
        # dict_euler['euler'] = dict_euler.pop('E')
        return dict_euler[cat]


def init_i2c(d_i2c, arduino_addr, frequency, setting_file):
    time.sleep(20)
    logger.info("I2C Arduino...")
    c = Talky(arduino_addr, setting_file)
    c.get_distances()
    c.get_minimu()
    while True:
        distances = c.get_distances()
        logger.info(distances)
        sensors = c.get_minimu()
        logger.info(sensors)
        time.sleep(frequency)


if __name__ == '__main__':
    log = logging.info("Start")
    addr = "0x12"
    addr = int(addr, 16)
    c = Talky(addr, "RTIMULib")
    # distances = c.get_distances()
    # sensors = c.get_minimu()

    sensors = c.get_minimu()
    print(sensors)
