#!/usr/bin/env python
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of R1D3.
#
# R1D3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# R1D3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with R1D3.  If not, see <http://www.gnu.org/licenses/>.
#

import configparser


class ConfHandler:
    def __init__(self, path_ini):
        self.p = path_ini

    def config_loader(self):
        config = configparser.ConfigParser()
        config.read(self.p)
        return config

    @staticmethod
    def configsectionmap(config, section):
        dict1 = {}
        options = config.options(section)
        for option in options:
            try:
                dict1[option] = config.get(section, option)
                if dict1[option] == -1:
                    print("skip: %s" % option)
            except:
                print("exception on %s!" % option)
                dict1[option] = None
        return dict1

    def write_config(self):
        cfgfile = open(self.p, 'w')
        config = configparser.ConfigParser()
        config.add_section('Config')
        config.set('Config', 'window', 'true')
        config.set('Config', 'debug', 'DEBUG')

        # config.add_section('I2C')
        # config.set('Config', 'arduino_i2c', 'false')
        # config.set('Config', 'arduino_addr', '0x12')
        # config.set('Config', 'arduino_frequency', '15')

        config.add_section('Camera')
        config.set('Camera', 'cam_num', 0)
        config.set('Camera', 'res_h', 480)
        config.set('Camera', 'res_w', 640)
        config.set('Camera', 'movie', 'false')

        config.add_section('Faces')
        config.set('Faces', 'faceset', 'lbpcascade_frontalface.xml')
        config.set('Faces', 'fistset', 'fist.xml')
        config.set('Faces', 'otherset', 'palm.xml')

        config.add_section('XMPP')
        config.set('XMPP', 'xmpp', 'true')
        config.set('XMPP', 'robot_jid', 'robot@example.com')
        config.set('XMPP', 'password', 'pass')
        config.set('XMPP', 'to_jid', 'me@example.com')
        config.set('XMPP', 'xmpp.register_plugin', "xep_0030 xep_0199 ", "xep_0004", "xep_0045", "xep_0050")

        config.add_section('SLAM')
        config.set('SLAM', 'slam', 'true')
        config.set('SLAM', 'map_size_pixel', 500)
        config.set('SLAM', 'map_size_meters', 10)
        config.set('SLAM', 'device', '/dev/ttyUSB0')
        config.set('SLAM', 'min_samples', 250)

        config.write(cfgfile)
        cfgfile.close()

    def read_config(self):
        section = 'Config'
        c = self.config_loader()
        window = c.getboolean(section, 'window')
        log_level = c.get(section, 'debug')
        battery_pattern = c.get(section, 'battery_pattern')
        battery_threshold = c.getint(section, 'battery_threshold')
        print(section, window, log_level, battery_pattern, battery_threshold)

        # section = 'I2C'
        # arduino_i2c = c.getboolean(section, 'arduino_i2c')
        # arduino_addr = c.get(section, 'arduino_addr')
        # arduino_freq = c.getint(section, 'frequency')
        # print(section, arduino_i2c, arduino_addr, arduino_freq)

        section = 'SERIAL'
        serial = c.getboolean(section, 'serial')
        address = c.get(section, 'address')
        speed = c.getint(section, 'speed')

        section = 'Camera'
        cam_num = c.getint(section, 'cam_num')
        resh = c.getint(section, 'res_h')
        resw = c.getint(section, 'res_w')
        movie = c.getboolean(section, 'movie')
        movie_path = c.get(section, 'movie_path')
        auto_movie = c.getboolean(section, 'auto_movie')
        print(section, resh, resw, movie, auto_movie)
        section = 'Faces'
        faceset = c.get(section, 'faceset')
        fistset = c.get(section, 'fistset')
        otherset = c.get(section, 'otherset')
        print(section, faceset, fistset, otherset)
        section = 'XMPP'
        xmpp_enable = c.getboolean(section, 'xmpp')
        robot_jid = c.get(section, 'robot_jid')
        passwd = c.get(section, 'password')
        to_jid = c.get(section, 'to_jid')
        room = c.get(section, 'muc_room')
        nick = c.get(section, 'muc_nick')
        xmpp_register_plugins = c.get(section, 'xmpp.register_plugin')
        print(section, xmpp_enable, robot_jid, to_jid, xmpp_register_plugins)
        section = 'LCD'
        lcd_enable = c.getboolean(section, 'lcd')
        print(section, lcd_enable)
        section = 'SLAM'
        slam = c.get(section, 'slam')
        map_size_pixel = c.getint(section, 'map_size_pixel')
        map_size_meters = c.getint(section, 'map_size_meters')
        device = c.get(section, 'device')
        min_samples = c.getint(section, 'min_samples')
        output_file = c.get(section, 'output_file')
        print(section, slam, map_size_pixel, map_size_meters, device, min_samples, output_file)
        section = 'BLUETOOTH'
        blue = c.getboolean(section, 'blue')
        print(section, blue)
        r = {'config': {'window': window, 'log_level': log_level, 'battery_pattern': battery_pattern,
                        'battery_threshold': battery_threshold},
             'camera': {'cam_num': cam_num,'resh': resh, 'resw': resw, 'movie': movie, 'movie_path': movie_path,
                        'auto_movie': auto_movie},
             'faces': {'faceset': faceset, 'fistset': fistset, 'otherset': otherset},
             'xmpp': {'enabled': xmpp_enable, 'robot_jid': robot_jid, 'to_jid': to_jid, 'password': passwd,
                      'room': room, 'nick': nick, 'xmpp_register_plugins': xmpp_register_plugins},
             'lcd': lcd_enable,
             'slam': {'enabled': slam, 'map_size_pixel': map_size_meters, 'device': device, 'min_samples': min_samples,
                      'output_file': output_file},
             'serial': {'enabled': serial, 'address': address, 'speed': speed}, 'blue': {'enabled': blue}
             }
        return r


if __name__ == '__main__':
    # Setup the command line arguments.
    path = '../r1d3.ini'
    s = ConfHandler(path)
    s.write_config()
    # s.read_config()
