#!/usr/bin/env python
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of R1D3.
#
# R1D3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# R1D3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with R1D3.  If not, see <http://www.gnu.org/licenses/>.
#


import serial
import logging


class SerialCom:
    def __init__(self):
        self.ser = None

    def start_com(self, s_port, speed, tm):
        logger = logging.getLogger('root')
        self.ser = serial.Serial(s_port, speed, timeout=tm)
        logger.info('Serial COM. Activated')


    def send_ser(self, msg):
        logger = logging.getLogger('root')
        self.ser.write(msg)
        logger = logging.getLogger('root')
        logger.info('SND MSG: ' + msg)

    def rcv_ser(self):
        logger = logging.getLogger('root')
        rcv = self.ser.readline()
        logger.info('RCV MSG: ' + rcv)
        return rcv


