#!/usr/bin/env python
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of R1D3.
#
# R1D3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# R1D3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with R1D3.  If not, see <http://www.gnu.org/licenses/>.
#

from multiprocessing import Process, Manager
import sys
import logging
import os
import time
import linecache
import glob
import subprocess

from .addons import xmpp
from .addons import video
from .addons import lcd
from .addons import chaussette
from .addons import mapping
from .addons import arduino_serial
from .addons import bluetooh
from .addons import system

from . import config
from . import misc

logging.basicConfig()

# TODO: Tread to isolate the loop. Other Tread can stop it (e.g. a button is pressed)
logger = logging.getLogger('root')


def file_test(path, t):
    if os.path.exists(path):
        return True
    else:
        m = path + ' does not exist \nPlease create ' + path + ' ' + t + ' in the robot.py directory.'
        logger.error(m)


class Robot:
    def __init__(self, root):
        self.root = root
        self.WINDOWS = False
        self.arduino_i2c = False
        self.XMPP = False
        self.lcd_enabled = False
        self.slam = False
        self.blue = False
        self.serial = False
        self.traget_detected = 0  # tracking mode or not
        self.checkpoint = 1
        self.prevac = 0
        self.facedetect = 0
        self.shared_vars = False
        self.lcd_instance = None

    def handle_exception(self):
        exc_type, exc_obj, tb = sys.exc_info()
        f = tb.tb_frame
        lineno = tb.tb_lineno
        filename = f.f_code.co_filename
        linecache.checkcache(filename)
        line = linecache.getline(filename, lineno, f.f_globals)
        m = 'EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj)
        logger.error(m)

    def modules(self, conf):
        # FIXME: replace all these lists with nice dictionnaries
        if conf['config']['window']:
            # X11 windows can be opened
            self.WINDOWS = True
        if conf['xmpp']['enabled']:
            # XMPP communications enabled
            self.XMPP = True
        if conf['lcd']:
            # LCD Char plate enabled
            self.lcd_enabled = True
        if conf['slam']['enabled']:
            # rplidar is connected and can be used
            self.slam = True
        if conf['serial']['enabled']:
            self.serial = True
        if conf['blue']['enabled']:
            self.blue = True

    def launch(self, path='r1d3.ini'):
        misc_instance = misc.Misc()
        self.shared_vars = misc.Share()
        colors = misc.Colors()
        colors = vars(colors)
        manager = Manager()
        d_lcd = manager.dict()
        d_xmpp = manager.dict()
        d_slam = manager.dict()
        d_mode = manager.dict()
        d_detect = manager.dict()
        d_ser = manager.dict()
        d_socket = manager.dict()

        # set all variables to false
        self.shared_vars.set_vars()
        # set all dictionary entries for xmpp to false
        v_share = {'to': ' ', 'msg': ' ', 'status': ' ', 'status_msg': ' ', 'send_dict': '0', 'roger': '0',
                   'online': True}
        d_xmpp = self.update_dict(d_xmpp, v_share)
        mode_skeleton = vars(self.shared_vars)
        d_mode = self.update_dict(d_mode, mode_skeleton)
        conf = None
        proc_xmpp = None
        proc_lcd = None
        proc_detection = None
        try:
            path = self.root + path
            file_test(path, 'file')
            c = config.ConfHandler(path)
            conf = c.read_config()
            # DEBUG, ERROR, WARNING ETC
            logger = misc_instance.setup_custom_logger('root', conf['config']['log_level'])

            self.modules(conf)
            self.shared_vars.set_listening_vars()

            # TODO LOOP System
            #     [root@agayon xuser]# dmesg  |grep Under
            #     [  794.815830] Under-voltage detected! (0x00050005)
            #     [root@agayon xuser]# journalctl --since=today |grep Under
            #     Jul 12 15:18:44 agayon kernel: Under-voltage detected! (0x00050005)
            #     [root@agayon xuser]# date
            #     Sun 12 Jul 2020 15:37:18 UTC
            #     [root@agayon xuser]#
            # TODO run journalctl regularly, regex time, compare with now. if more than x under voltage and latest sooner than x
            # TODO stop and shutdown safely with a message on the Arduino matrix :-)

            # System loop
            system_options = {'battery_pattern': conf['config'].get('battery_pattern'),
                              'battery_threshold': conf['config'].get('battery_threshold')}
            keywords = {'d_ser': d_ser, 'd_xmpp': d_xmpp, 'd_mode': d_mode, 'd_lcd': d_lcd,
                        'mode_skeleton': mode_skeleton, 'd_socket': d_socket,'system_options': system_options}
            proc_system = Process(target=system.system.init_system,
                                kwargs=keywords)
            proc_system.start()

            if self.lcd_enabled:
                dict_lcd_skeleton = {'msg': ' ', 'color': ' ', 'menu': ' '}
                d_lcd = self.update_dict(d_lcd, dict_lcd_skeleton)
                keywords = {'d_lcd': d_lcd, 'd_mode': d_mode, 'd_xmpp': d_xmpp, 'mode_skeleton': mode_skeleton}
                proc_lcd = Process(target=lcd.menu_r1d3.lcd_start, kwargs=keywords)
                proc_lcd.start()

            if self.XMPP:
                keywords = {'d_xmpp': d_xmpp, 'd_mode': d_mode, 'd_lcd': d_lcd, 'mode_skeleton': mode_skeleton,
                            'jid': conf['xmpp']['robot_jid'], 'to_jid': conf['xmpp']['to_jid'],
                            'password': conf['xmpp']['password'], 'room': conf['xmpp']['room'],
                            'nick': conf['xmpp']['nick'],
                            'xmpp_register_plugins': conf['xmpp']['xmpp_register_plugins']
                            }
                proc_xmpp = Process(target=xmpp.xmpp.init_xmpp,
                                    kwargs=keywords)
                proc_xmpp.start()

            if self.slam:
                keywords = {'d_lcd': d_lcd, 'd_mode': d_mode, 'd_xmpp': d_xmpp, 'd_slam': d_slam, 'd_ser': d_ser,
                            'mode_skeleton': mode_skeleton,
                            'slam': conf['slam']['enabled'], 'map_size_meters': conf['slam']['map_size_pixel'],
                            'map_size_pixel': conf['slam']['map_size_pixel'],
                            'device_address': conf['slam']['device'], 'min_samples': conf['slam']['min_samples'],
                            'output_file': conf['slam']['output_file']}
                proc_slam = Process(target=mapping.slagayon.slam_start,
                                    kwargs=keywords)
                proc_slam.start()

            if self.serial:
                keywords = {'d_mode': d_mode, 'd_ser': d_ser, 'd_slam': d_slam, 'mode_skeleton': mode_skeleton,
                            'address': conf['serial']['address'], 'speed': conf['serial']['speed']}
                proc_serial = Process(target=arduino_serial.arduino_serial.init_serial,
                                      kwargs=keywords)
                proc_serial.start()

            if self.blue:
                keywords = {'d_mode': d_mode, 'd_ser': d_ser, 'mode_skeleton': mode_skeleton, 'd_lcd': d_lcd}
                proc_blue = Process(target=bluetooh.bluetooth_ps4.init_bluetooth,
                                    kwargs=keywords)
                proc_blue.start()

            keywords = {'d_ser': d_ser, 'd_xmpp': d_xmpp, 'd_mode': d_mode, 'd_lcd': d_lcd,
                        'mode_skeleton': mode_skeleton, 'd_socket': d_socket}
            proc_socket = Process(target=chaussette.socket_server.init_socket,
                                  kwargs=keywords)
            proc_socket.start()

        except Exception as e:
            logger.error(e)
            self.handle_exception()

        if not conf:
            logging.critical("No valid config file found. Please create r1d3.ini.")
            return 1
        window = conf['config']['window']
        faces_path = self.root + "known_faces"
        file_test(faces_path, 'directory')
        scale = 1
        interval = 0.1
        camera_num = conf['camera']['cam_num']
        resh = conf['camera']['resh']
        resw = conf['camera']['resw']
        movie = conf['camera']['movie']
        movie_path = conf['camera']['movie_path']
        auto_movie = conf['camera']['auto_movie']
        if auto_movie:
            mode_skeleton['frame_capture'] = True
            mode_skeleton['video_mode'] = True
            mode_skeleton['sign_tracking'] = False
            mode_skeleton['shared_vars_from_xmpp'] = False
            mode_skeleton['mapping_mode'] = False
            mode_skeleton['facedetect_mode'] = False
            self.update_dict(d_mode, mode_skeleton)

        path = self.root + "/cascades/cascades-enabled/"
        file_test(path, 'directory')
        cascpath = []
        files = glob.glob(self.root + "cascades/cascades-enabled/*.xml")
        for f in files:
            cascpath.append(f)
        capture_session = False
        while 1:
            try:
                # print('###############################')
                for v in ('shared_vars_from_lcd', 'shared_vars_from_xmpp', 'frame_capture', 'video_mode', 'follow_mode',
                          'mapping_mode',
                          'facedetect_mode', 'sign_tracking', 'sign_phony'):
                    if d_mode[v] != False:
                        try:
                            print(v + ' ' + str(d_mode[v]))
                        except KeyError:
                            print("Key error d_mode")
                if d_mode['frame_capture'] is True and capture_session is False:
                    # Stop the capture
                    mode_skeleton['stream'] = False
                    stop_stream_args = ["/usr/local/mybin/stop_stream.sh"]
                    time.sleep(1)
                    subprocess.run(stop_stream_args, capture_output=False)
                    # Start python stream
                    mode_skeleton_new = self.update_dict(d_mode, mode_skeleton)
                    target_detected = self.traget_detected
                    checkpoint = self.checkpoint
                    facedetect = self.facedetect
                    prevac = self.prevac
                    proc_detection = Process(target=video.detect.init_detection, args=(interval, window, d_mode,
                                                                                       d_detect, d_xmpp, d_lcd,
                                                                                       resh, resw, target_detected,
                                                                                       checkpoint, prevac, facedetect,
                                                                                       scale, camera_num, cascpath,
                                                                                       faces_path, movie, movie_path,
                                                                                       colors, mode_skeleton_new))
                    proc_detection.start()
                    capture_session = True
                if not d_mode.get('frame_capture') and d_mode.get('video_reset'):
                # FIXME This section reinitialize automatically the skeleton.
                # This causes problems with remote = true
                    capture_session = False
                    mode_skeleton['frame_capture'] = False
                    mode_skeleton['video_mode'] = False
                    mode_skeleton['stream'] = False
                    mode_skeleton['facedetect_mode'] = False
                    mode_skeleton['sign_tracking'] = False
                    mode_skeleton['sign_phony'] = False
                    mode_skeleton['sign_phony'] = False
                    mode_skeleton['shared_vars_from_lcd'] = False
                    mode_skeleton['shared_vars_from_xmpp'] = False
                    mode_skeleton['video_reset'] = False
                    mode_skeleton['remote'] = d_mode.get('remote')
                    d_mode = self.update_dict(d_mode, mode_skeleton)

                time.sleep(5)
            except KeyboardInterrupt:
                logger.info('Quit')
                d_xmpp_close = {'to': ' ', 'msg': ' ', 'status': ' ', 'status_msg': ' ', 'roger': '1', 'send_dict': '0',
                                'online': False}
                try:
                    d_xmpp = self.update_dict(d_xmpp, d_xmpp_close)
                except ConnectionResetError:
                    time.sleep(5)
                    proc_slam = None
                    for proc in [proc_detection, proc_lcd, proc_slam, proc_xmpp, proc_blue, proc_serial, proc_slam,
                                 proc_socket, proc_serial]:
                        try:
                            proc.terminate()
                        except AttributeError:
                            pass
                logger.info('Exit')
                sys.exit(0)

    def update_dict(self, input_manager_dict, new_dict):
        """
        Little trick to force the update of the manager.dict
        :param input_manager_dict: the dictionary to update
        :param new_dict: a simple dictionary containing the values
        :return: the manager.dictionary
        """
        dict_temp = input_manager_dict
        for key, value in new_dict.items():
            dict_temp[key] = value
        input_manager_dict = dict_temp
        return input_manager_dict


if __name__ == '__main__':
    # Setup the command line arguments.
    r = Robot('../')
    r.launch('r1d3.ini')
