In order to detect some objects, OpenCV needs some xml files.
These files should be placed in the directory named "cascades-available".
The cascades files that need to be used sould be placed in the directory named "cascades-enabled".


The cascadefiles have been found on several locations:

[fist](https://github.com/Aravindlivewire/Opencv/blob/master/haarcascade/fist.xml)

[palm](https://github.com/Aravindlivewire/Opencv/blob/master/haarcascade/palm.xml)

[closed frontal palm](https://github.com/Aravindlivewire/Opencv/blob/master/haarcascade/closed_frontal_palm.xml)

[lbpcascade frontalface](https://github.com/Itseez/opencv/blob/master/data/lbpcascades/lbpcascade_frontalface.xml)

